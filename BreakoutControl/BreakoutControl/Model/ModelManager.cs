﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BreakoutControl
{
    public static class ModelManager
    {
        private static Logger log = new Logger(typeof(ModelManager).ToString());

        private static string path;
        private static string filename = "model.json";
        static Model model;

        public static void CreateSettings()
        {
            log.info("settings start create");
            string assemblyName = Assembly.GetExecutingAssembly().GetName().Name;
            path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + assemblyName + "\\";
            Model currentSettings = null;
            if (File.Exists(path + filename) == false)
            {
                currentSettings = new DefaultGames();
                setSettings(currentSettings);
            }
            else
            {
                currentSettings = readSettings();
                if (currentSettings == null)
                {
                    string s = JsonConvert.SerializeObject(new DefaultGames());
                    currentSettings = JsonConvert.DeserializeObject<Model>(s);
                    currentSettings = new DefaultGames();
                    setSettings(currentSettings);
                }
                else setSettings(currentSettings);
            }

            model = currentSettings;
            log.info("settings create finish");
        }
        public static Model getCurrentSettings()
        {
            return model;
        }
        private static Model readSettings()
        {
            try
            {
                string settings = "";
                using (StreamReader readtext = new StreamReader(path + filename))
                {
                    string line;
                    while ((line = readtext.ReadLine()) != null) settings += line;
                }
                return JsonConvert.DeserializeObject<Model>(settings);
            }
            catch (Exception) { return null; }
        }

        private static void setSettings(Model ds)
        {
            string settings = JsonConvert.SerializeObject(ds, Formatting.Indented);
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
            using (StreamWriter sw = new StreamWriter(path + filename, false))
            {
                sw.WriteLine(settings);
                sw.Close();
            }
        }
    }
}
