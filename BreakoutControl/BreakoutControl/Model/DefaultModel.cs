﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BreakoutControl
{
    public class CONTROL : Model.CONTROL_GAME
    {
        public CONTROL()
        {
            this.name = "Controls";
            this.Sections = new List<CONTROL_SECTION>();
            this.Sections.Add(new Update_Info());
            this.Sections.Add(new Control_game());
        }
        public class Update_Info : Model.CONTROL_GAME.CONTROL_SECTION
        {
            public Update_Info()
            {
                this.name = "Update info";
                this.Actions = new List<CONTROL_ACTION>();
                this.Actions.Add(new Auto_update());
                this.Actions.Add(new Manual_update());
            }
            public class Auto_update : Model.CONTROL_GAME.CONTROL_SECTION.CONTROL_ACTION
            {
                public Auto_update()
                {
                    this.id = 1;
                    this.changeable = true;
                    this.changeName = "Disconnect";
                    this.name = "Auto";
                    this.pollFlag = true;
                    //this.command = new int[2] { 81, 20 };
                    this.commandname = "AUTO";
                    this.changecommandname = "DISCONNECT";
                }
            }
            public class Manual_update : Model.CONTROL_GAME.CONTROL_SECTION.CONTROL_ACTION
            {
                public Manual_update()
                {
                    this.id = 2;
                    this.changeable = false;
                    this.changeName = "";
                    this.name = "Manual";
                    this.pollFlag = false;
                    //this.command = new int[2] { 81, 20 };
                    this.commandname = "MANUAL";
                    this.changecommandname = "";
                }
            }
        }
        public class Control_game : Model.CONTROL_GAME.CONTROL_SECTION
        {
            public Control_game()
            {
                this.name = "Control game";
                this.Actions = new List<CONTROL_ACTION>();
                this.Actions.Add(new Start_game());
                this.Actions.Add(new Stop_game());
                this.Actions.Add(new Restart_game());
                this.Actions.Add(new Check_Connection());
            }
            public class Start_game : Model.CONTROL_GAME.CONTROL_SECTION.CONTROL_ACTION
            {
                public Start_game()
                {
                    this.id = 3;
                    this.changeable = false;
                    this.changeName = "";
                    this.name = "Start game";
                    this.pollFlag = false;
                    //this.command = new int[1] { 17 };
                    this.commandname = "START";
                    this.changecommandname = "";
                }
            }
            public class Stop_game : Model.CONTROL_GAME.CONTROL_SECTION.CONTROL_ACTION
            {
                public Stop_game()
                {
                    this.id = 4;
                    this.changeable = false;
                    this.changeName = "";
                    this.name = "Stop game";
                    this.pollFlag = false;
                    //this.command = new int[1] { 18 };
                    this.commandname = "STOP";
                    this.changecommandname = "";
                }
            }
            public class Restart_game : Model.CONTROL_GAME.CONTROL_SECTION.CONTROL_ACTION
            {
                public Restart_game()
                {
                    this.id = 5;
                    this.changeable = false;
                    this.changeName = "";
                    this.name = "Restart game";
                    this.pollFlag = false;
                    //this.command = new int[1] { 19 };
                    this.commandname = "RESTART";
                    this.changecommandname = "";
                }
            }
            public class Check_Connection : Model.CONTROL_GAME.CONTROL_SECTION.CONTROL_ACTION
            {
                public Check_Connection()
                {
                    this.id = 6;
                    this.changeable = false;
                    this.changeName = "";
                    //this.command = new int[1] { 16 };
                    this.name = "Ping";
                    this.pollFlag = false;
                    this.commandname = "PING";
                    this.changecommandname = "";
                }
            }
        }
    }
    public class DraculaGame : Model.GAME
    {
        public DraculaGame()
        {
            this.idgame = 1;
            this.mcadr = 2;
            this.name = "Dracula";
            this.shortName = "Dracula";
            this.Sections = new List<SECTION>();
            this.Sections.Add(new Cache1());
            this.Sections.Add(new Crypt());
            this.Sections.Add(new Cache2());
            this.Sections.Add(new Armchair());
            this.Sections.Add(new Stand());
            this.Sections.Add(new Gate());
            this.Sections.Add(new Exit_door());
            this.Sections.Add(new Lightning_rooms());
        }
        public class Cache1 : Model.GAME.SECTION
        {
            public Cache1()
            {
                this.idsection = 1;
                this.name = "Cache1";
                this.Actions = new List<DIRECT_ACTION>();
                this.Tasks = new List<TASK>();
                this.Tasks.Add(new Activation_key());
            }
            public class Activation_key : Model.GAME.SECTION.TASK
            {
                public Activation_key()
                {
                    this.idtask = 1;
                    this.Event = new Activation_k();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Open_cache1());
                }
                public class Activation_k : Model.GAME.SECTION.TASK.EVENT
                {
                    public Activation_k()
                    {
                        this.idevent = 1;
                        this.name = "Activation key";
                    }
                }
                public class Open_cache1 : Model.GAME.SECTION.TASK.ACTION
                {
                    public Open_cache1()
                    {
                        this.name = "Open cache 1";
                        this.idaction = 1;
                        this.command = 0x31;
                    }
                }
            }
        }
        public class Crypt : Model.GAME.SECTION
        {
            public Crypt()
            {
                this.idsection = 2;
                this.name = "Crypt";
                this.Actions = new List<DIRECT_ACTION>();
                this.Tasks = new List<TASK>();
                this.Tasks.Add(new Buttons());
                this.Tasks.Add(new Dracula());
            }
            public class Buttons : Model.GAME.SECTION.TASK
            {
                public Buttons()
                {
                    this.idtask = 2;
                    this.Event = new Buttons_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Open_crypt());
                }
                public class Buttons_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Buttons_event()
                    {
                        this.idevent = 2;
                        this.name = "Buttons";
                    }
                }
                public class Open_crypt : Model.GAME.SECTION.TASK.ACTION
                {
                    public Open_crypt()
                    {
                        this.command = 0x32;
                        this.idaction = 2;
                        this.name = "Open crypt";
                    }
                }
            }
            public class Dracula : Model.GAME.SECTION.TASK
            {
                public Dracula()
                {
                    this.idtask = 3;
                    this.Event = new Dracula_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Open_cache_crypt());
                }
                public class Dracula_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Dracula_event()
                    {
                        this.idevent = 3;
                        this.name = "Dracula";
                    }
                }
                public class Open_cache_crypt : Model.GAME.SECTION.TASK.ACTION
                {
                    public Open_cache_crypt()
                    {
                        this.command = 0x33;
                        this.idaction = 3;
                        this.name = "Open cache crypt";
                    }
                }
            }
        }
        public class Cache2 : Model.GAME.SECTION
        {
            public Cache2()
            {
                this.idsection = 3;
                this.name = "Cache 2";
                this.Actions = new List<DIRECT_ACTION>();
                this.Tasks = new List<TASK>();
                this.Tasks.Add(new Bodies());
                this.Tasks.Add(new Vessel());
            }
            public class Bodies : Model.GAME.SECTION.TASK
            {
                public Bodies()
                {
                    this.idtask = 4;
                    this.Event = new Bodies_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Open_cache2());
                    this.Actions.Add(new Close_cache2());
                }
                public class Bodies_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Bodies_event()
                    {
                        this.idevent = 4;
                        this.name = "Bodies";
                    }
                }
                public class Open_cache2 : Model.GAME.SECTION.TASK.ACTION
                {
                    public Open_cache2()
                    {
                        this.command = 0x34;
                        this.idaction = 4;
                        this.name = "Open cache 2";
                    }
                }
                public class Close_cache2: Model.GAME.SECTION.TASK.ACTION
                {
                    public Close_cache2()
                    {
                        this.command = 0x35;
                        this.idaction = 5;
                        this.name = "Close cache 2";
                    }
                }
            }
            public class Vessel : Model.GAME.SECTION.TASK
            {
                public Vessel()
                {
                    this.idtask = 5;
                    this.Event = new Vessel_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Open_cache3());
                }
                public class Vessel_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Vessel_event()
                    {
                        this.name = "Vessel";
                        this.idevent = 5;
                    }
                }
                public class Open_cache3 : Model.GAME.SECTION.TASK.ACTION
                {
                    public Open_cache3()
                    {
                        this.command = 0x36;
                        this.idaction = 6;
                        this.name = "Open cache 3";
                    }
                }

            }
        }
        public class Armchair : Model.GAME.SECTION
        {
            public Armchair()
            {
                this.idsection = 4;
                this.name = "Armchair";
                this.Actions = new List<DIRECT_ACTION>();
                this.Tasks = new List<TASK>();
                this.Tasks.Add(new Seat_sensor());
                this.Tasks.Add(new Figurines_position());
            }
            public class Seat_sensor : Model.GAME.SECTION.TASK
            {
                public Seat_sensor()
                {
                    this.idtask = 6;
                    this.Event = new Seat_sensor_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Block_figurines());
                    this.Actions.Add(new Disblock_figurines());
                }
                public class Seat_sensor_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Seat_sensor_event()
                    {
                        this.idevent = 6;
                        this.name = "Seat sensor";
                    }
                }
                public class Block_figurines : Model.GAME.SECTION.TASK.ACTION
                {
                    public Block_figurines()
                    {
                        this.command = 0x38;
                        this.idaction = 7;
                        this.name = "Block figurines";
                    }
                }
                public class Disblock_figurines : Model.GAME.SECTION.TASK.ACTION
                {
                    public Disblock_figurines()
                    {
                        this.command = 0x37;
                        this.idaction = 8;
                        this.name = "Disblock figurines";
                    }
                }
            }
            public class Figurines_position : Model.GAME.SECTION.TASK
            {
                public Figurines_position()
                {
                    this.idtask = 7;
                    this.Event = new Figurines_position_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Open_cache_armchair());
                }
                public class Figurines_position_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Figurines_position_event()
                    {
                        this.idevent = 7;
                        this.name = "Figurines position";
                    }
                }
                public class Open_cache_armchair : Model.GAME.SECTION.TASK.ACTION
                {
                    public Open_cache_armchair()
                    {
                        this.command = 0x39;
                        this.idaction = 9;
                        this.name = "Open cache armchair";
                    }
                }
            }
        }
        public class Stand : Model.GAME.SECTION
        {
            public Stand()
            {
                this.idsection = 5;
                this.Actions = new List<DIRECT_ACTION>();
                this.name = "Stand";
                this.Tasks = new List<TASK>();
                this.Tasks.Add(new Icons());
            }
            public class Icons : Model.GAME.SECTION.TASK
            {
                public Icons()
                {
                    this.idtask = 8;
                    this.Event = new Icons_events();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Move_stand());
                }
                public class Icons_events : Model.GAME.SECTION.TASK.EVENT
                {
                    public Icons_events()
                    {
                        this.name = "Icons";
                        this.idevent = 8;
                    }
                }
                public class Move_stand : Model.GAME.SECTION.TASK.ACTION
                {
                    public Move_stand()
                    {
                        this.command = 0x3A;
                        this.idaction = 10;
                        this.name = "Move stand";
                    }
                }
            }
        }
        public class Gate : Model.GAME.SECTION
        {
            public Gate()
            {
                this.name = "Gate";
                this.idsection = 6;
                this.Actions = new List<DIRECT_ACTION>();
                this.Tasks = new List<TASK>();
                this.Tasks.Add(new Levers());
                this.Tasks.Add(new Open_sensor());
                this.Tasks.Add(new Close_sensor());
                this.Tasks.Add(new Ray_sensor());
            }
            public class Levers : Model.GAME.SECTION.TASK
            {
                public Levers()
                {
                    this.idtask = 9;
                    this.Event = new Levers_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Open_gate());
                    this.Actions.Add(new Close_gate());
                }
                public class Levers_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Levers_event()
                    {
                        this.idevent = 9;
                        this.name = "Levers";
                    }
                }
                public class Open_gate : Model.GAME.SECTION.TASK.ACTION
                {
                    public Open_gate()
                    {
                        this.name = "Open gate";
                        this.idaction = 11;
                        this.command = 0x3B;
                    }
                }
                public class Close_gate : Model.GAME.SECTION.TASK.ACTION
                {
                    public Close_gate()
                    {
                        this.command = 0x3C;
                        this.idaction = 12;
                        this.name = "Close gate";
                    }
                }
            }
            public class Open_sensor : Model.GAME.SECTION.TASK
            {
                public Open_sensor()
                {
                    this.Actions = new List<ACTION>();
                    this.Event = new Open_sensor_event();
                    this.idtask = 10;
                }
                public class Open_sensor_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Open_sensor_event()
                    {
                        this.idevent = 10;
                        this.name = "Open sensor";
                    }
                }
            }
            public class Close_sensor : Model.GAME.SECTION.TASK
            {
                public Close_sensor()
                {
                    this.Actions = new List<ACTION>();
                    this.Event = new Close_sensor_event();
                    this.idtask = 11;
                }
                public class Close_sensor_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Close_sensor_event()
                    {
                        this.idevent = 11;
                        this.name = "Close sensor";
                    }
                }
            }
            public class Ray_sensor : Model.GAME.SECTION.TASK
            {
                public Ray_sensor()
                {
                    this.Actions = new List<ACTION>();
                    this.Event = new Ray_sensor_event();
                    this.idtask = 12;
                }
                public class Ray_sensor_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Ray_sensor_event()
                    {
                        this.idevent = 12;
                        this.name = "Ray sensor";
                    }
                }
            }
        }
        public class Exit_door : Model.GAME.SECTION
        {
            public Exit_door()
            {
                this.name = "Exit door";
                this.idsection = 7;
                this.Actions = new List<DIRECT_ACTION>();
                this.Tasks = new List<TASK>();
                this.Tasks.Add(new Exit_RFID());
                this.Tasks.Add(new Open_sensor());
                this.Tasks.Add(new Close_sensor());
            }
            public class Exit_RFID : Model.GAME.SECTION.TASK
            {
                public Exit_RFID()
                {
                    this.idtask = 13;
                    this.Event = new Exit_rfid_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Open_door());
                    this.Actions.Add(new Close_door());
                }
                public class Exit_rfid_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Exit_rfid_event()
                    {
                        this.idevent = 13;
                        this.name = "Exit RFID";
                    }
                }
                public class Open_door : Model.GAME.SECTION.TASK.ACTION
                {
                    public Open_door()
                    {
                        this.command = 0x3D;
                        this.idaction = 13;
                        this.name = "Open door";
                    }
                }
                public class Close_door : Model.GAME.SECTION.TASK.ACTION
                {
                    public Close_door()
                    {
                        this.command = 0x3E;
                        this.idaction = 14;
                        this.name = "Close door";
                    }
                }
            }
            public class Open_sensor : Model.GAME.SECTION.TASK
            {
                public Open_sensor()
                {
                    this.idtask = 14;
                    this.Event = new Open_sensor_event();
                    this.Actions = new List<ACTION>();
                }
                public class Open_sensor_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Open_sensor_event()
                    {
                        this.idevent = 14;
                        this.name = "Open sensor";
                    }
                }
            }
            public class Close_sensor : Model.GAME.SECTION.TASK
            {
                public Close_sensor()
                {
                    this.idtask = 15;
                    this.Event = new Close_sensor_event();
                    this.Actions = new List<ACTION>();
                }
                public class Close_sensor_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Close_sensor_event()
                    {
                        this.idevent = 15;
                        this.name = "Close sensor";
                    }
                }
            }
        }
        public class Lightning_rooms : Model.GAME.SECTION
        {
            public Lightning_rooms()
            {
                this.idsection = 8;
                this.name = "Lightning rooms";
                this.Tasks = new List<TASK>();
                this.Actions = new List<DIRECT_ACTION>();
                this.Actions.Add(new Light_Off());
                this.Actions.Add(new Light_On());
            }
            public class Light_On : Model.GAME.SECTION.DIRECT_ACTION
            {
                public Light_On()
                {
                    this.command = 0x3F;
                    this.idaction = 14;
                    this.name = "Light On";
                }
            }
            public class Light_Off : Model.GAME.SECTION.DIRECT_ACTION
            {
                public Light_Off()
                {
                    this.command = 0x40;
                    this.idaction = 15;
                    this.name = "Light Off";
                }
            }
        }
    }
    public class StalkerGame : Model.GAME
    {
        public StalkerGame()
        {
            this.idgame = 2;
            this.mcadr = 3;
            this.name = "Chernobyl";
            this.shortName = "Stalker";
            this.Sections = new List<SECTION>();
            this.Sections.Add(new Cache2());
            this.Sections.Add(new Sign());
            this.Sections.Add(new Electric_panel());
            this.Sections.Add(new Console1());
            this.Sections.Add(new Joystick());
            this.Sections.Add(new Console2());
            this.Sections.Add(new Lightning_rooms());
            this.Sections.Add(new Test());
            this.Sections.Add(new Cameras());
            this.Sections.Add(new Lightning());                
        }
        public class Cache2 : Model.GAME.SECTION
        {
            public Cache2()
            {
                this.idsection = 1;
                this.name = "Cache 2";
                this.Actions = new List<DIRECT_ACTION>();
                this.Tasks = new List<TASK>();
                this.Tasks.Add(new Two_buttons());
            }
            public class Two_buttons : Model.GAME.SECTION.TASK
            {
                public Two_buttons()
                {
                    this.idtask = 1;
                    this.Event = new Two_buttons_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Open_cache2());
                }
                public class Two_buttons_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Two_buttons_event()
                    {
                        this.idevent = 1;
                        this.name = "Two buttons";
                    }
                }
                public class Open_cache2 : Model.GAME.SECTION.TASK.ACTION
                {
                    public Open_cache2()
                    {
                        this.command = 0x31;
                        this.idaction = 1;
                        this.name = "Open cache 2";
                    }
                }
            }
        }
        public class Sign : Model.GAME.SECTION
        {
            public Sign()
            {
                this.idsection = 2;
                this.name = "Sign";
                this.Actions = new List<DIRECT_ACTION>();
                this.Tasks = new List<TASK>();
                this.Tasks.Add(new Sign_RFID());
            }
            public class Sign_RFID : Model.GAME.SECTION.TASK
            {
                public Sign_RFID()
                {
                    this.idtask = 2;
                    this.Event = new Sign_rfid_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Block_sign());
                    this.Actions.Add(new Disblock_sign());
                }
                public class Sign_rfid_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Sign_rfid_event()
                    {
                        this.idevent = 2;
                        this.name = "Sign RFID";
                    }
                }
                public class Block_sign : Model.GAME.SECTION.TASK.ACTION
                {
                    public Block_sign()
                    {
                        this.command = 0x32;
                        this.idaction = 2;
                        this.name = "Block sign";
                    }
                }
                public class Disblock_sign : Model.GAME.SECTION.TASK.ACTION
                {
                    public Disblock_sign()
                    {
                        this.command = 0x33;
                        this.idaction = 3;
                        this.name = "Disblock sign";
                    }
                }
            }
        }
        public class Electric_panel : Model.GAME.SECTION
        {
            public Electric_panel()
            {
                this.idsection = 3;
                this.name = "Electric panel";
                this.Actions = new List<DIRECT_ACTION>();
                this.Tasks = new List<TASK>();
                this.Tasks.Add(new Electric_panel_task());
            }
            public class Electric_panel_task : Model.GAME.SECTION.TASK
            {
                public Electric_panel_task()
                {
                    this.idtask = 3;
                    this.Event = new Electric_panel_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Open_rebus());
                }
                public class Electric_panel_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Electric_panel_event()
                    {
                        this.idevent = 3;
                        this.name = "Electric panel";
                    }
                }
                public class Open_rebus : Model.GAME.SECTION.TASK.ACTION
                {
                    public Open_rebus()
                    {
                        this.command = 0x34;
                        this.idaction = 4;
                        this.name = "Open rebus";
                    }
                }
            }
        }
        public class Console1 : Model.GAME.SECTION
        {
            public Console1()
            {
                this.idsection = 4;
                this.name = "Console 1";
                this.Actions = new List<DIRECT_ACTION>();
                this.Tasks = new List<TASK>();
                this.Tasks.Add(new Combination1());
                this.Tasks.Add(new Combination2());
            }
            public class Combination1 : Model.GAME.SECTION.TASK
            {
                public Combination1()
                {
                    this.idtask = 4;
                    this.Event = new Combination1_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Open_door1());
                }
                public class Combination1_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Combination1_event()
                    {
                        this.idevent = 4;
                        this.name = "Combination 1";
                    }
                }
                public class Open_door1 : Model.GAME.SECTION.TASK.ACTION
                {
                    public Open_door1()
                    {
                        this.command = 0x35;
                        this.idaction = 5;
                        this.name = "Open door 1";
                    }
                }
            }
            public class Combination2 : Model.GAME.SECTION.TASK
            {
                public Combination2()
                {
                    this.idtask = 5;
                    this.Event = new Combination2_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Open_door2());
                }
                public class Combination2_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Combination2_event()
                    {
                        this.idevent = 5;
                        this.name = "Combination 2";
                    }
                }
                public class Open_door2 : Model.GAME.SECTION.TASK.ACTION
                {
                    public Open_door2()
                    {
                        this.command = 0x36;
                        this.idaction = 6;
                        this.name = "Open door 2";
                    }
                }
            }
        }
        public class Joystick : Model.GAME.SECTION
        {
            public Joystick()
            {
                this.idsection = 5;
                this.name = "Joystick";
                this.Actions = new List<DIRECT_ACTION>();
                this.Tasks = new List<TASK>();
                this.Tasks.Add(new Joystick_left());
                this.Tasks.Add(new Joystick_right());
            }
            public class Joystick_left : Model.GAME.SECTION.TASK
            {
                public Joystick_left()
                {
                    this.idtask = 6;
                    this.Event = new Joystick_left_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Turn_left());
                }
                public class Joystick_left_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Joystick_left_event()
                    {
                        this.idevent = 6;
                        this.name = "Left";
                    }
                }
                public class Turn_left : Model.GAME.SECTION.TASK.ACTION
                {
                    public Turn_left()
                    {
                        this.command = 0x37;
                        this.idaction = 7;
                        this.name = "Turn left";
                    }
                }
            }
            public class Joystick_right : Model.GAME.SECTION.TASK
            {
                public Joystick_right()
                {
                    this.idtask = 7;
                    this.Event = new Joystick_right_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Turn_right());
                }
                public class Joystick_right_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Joystick_right_event()
                    {
                        this.idevent = 7;
                        this.name = "Right";
                    }
                }
                public class Turn_right : Model.GAME.SECTION.TASK.ACTION
                {
                    public Turn_right()
                    {
                        this.command = 0x38;
                        this.idaction = 8;
                        this.name = "Turn right";
                    }
                }
            }
        }
        public class Console2 : Model.GAME.SECTION
        {
            public Console2()
            {
                this.idsection = 6;
                this.name = "Console 2";
                this.Actions = new List<DIRECT_ACTION>();
                this.Tasks = new List<TASK>();
                this.Tasks.Add(new Combination1());
                this.Tasks.Add(new Combination2());
                this.Tasks.Add(new Reactor());
            }
            public class Combination1 : Model.GAME.SECTION.TASK
            {
                public Combination1()
                {
                    this.idtask = 8;
                    this.Event = new Combination1_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Open_reactor());
                }
                public class Combination1_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Combination1_event()
                    {
                        this.idevent = 8;
                        this.name = "Combination 1";
                    }
                }
                public class Open_reactor : Model.GAME.SECTION.TASK.ACTION
                {
                    public Open_reactor()
                    {
                        this.command = 0x39;
                        this.idaction = 9;
                        this.name = "Open reactor";
                    }
                }
            }
            public class Combination2 : Model.GAME.SECTION.TASK
            {
                public Combination2()
                {
                    this.idtask = 9;
                    this.Event = new Combination2_event();
                    this.Actions = new List<ACTION>();
                }
                public class Combination2_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Combination2_event()
                    {
                        this.idevent = 9;
                        this.name = "Combination 2";
                    }
                }
            }
            public class Reactor : Model.GAME.SECTION.TASK
            {
                public Reactor()
                {
                    this.idtask = 10;
                    this.Event = new Reactor_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Open_exit_door());
                }
                public class Reactor_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Reactor_event()
                    {
                        this.idevent = 10;
                        this.name = "Reactor RFID";
                    }
                }
                public class Open_exit_door : Model.GAME.SECTION.TASK.ACTION
                {
                    public Open_exit_door()
                    {
                        this.command = 0x3A;
                        this.idaction = 10;
                        this.name = "Open exit door";
                    }
                }
            }
        }
        public class Cameras : Model.GAME.SECTION
        {
            public Cameras()
            {
                this.idsection = 7;
                this.name = "Cameras";
                this.Actions = new List<DIRECT_ACTION>();
                this.Actions.Add(new SwitchOffAll());
                this.Tasks = new List<TASK>();
                this.Tasks.Add(new Camera1());
                this.Tasks.Add(new Camera2());
                this.Tasks.Add(new Camera3());
                this.Tasks.Add(new Camera4());
            }
            public class Camera1 : Model.GAME.SECTION.TASK
            {
                public Camera1()
                {
                    this.idtask = 11;
                    this.Event = new Camera1_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Select_camera1());
                }
                public class Camera1_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Camera1_event()
                    {
                        this.idevent = 11;
                        this.name = "Camera 1";
                    }
                }
                public class Select_camera1 : Model.GAME.SECTION.TASK.ACTION
                {
                    public Select_camera1()
                    {
                        this.command = 0x3B;
                        this.idaction = 11;
                        this.name = "Select camera 1";
                    }
                }
            }
            public class Camera2 : Model.GAME.SECTION.TASK
            {
                public Camera2()
                {
                    this.idtask = 12;
                    this.Event = new Camera2_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Select_camera2());
                }
                public class Camera2_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Camera2_event()
                    {
                        this.idevent = 12;
                        this.name = "Camera 2";
                    }
                }
                public class Select_camera2 : Model.GAME.SECTION.TASK.ACTION
                {
                    public Select_camera2()
                    {
                        this.command = 0x3C;
                        this.idaction = 12;
                        this.name = "Select camera 2";
                    }
                }
            }
            public class Camera3 : Model.GAME.SECTION.TASK
            {
                public Camera3()
                {
                    this.idtask = 13;
                    this.Event = new Camera3_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Select_camera3());
                }
                public class Camera3_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Camera3_event()
                    {
                        this.idevent = 13;
                        this.name = "Camera 3";
                    }
                }
                public class Select_camera3 : Model.GAME.SECTION.TASK.ACTION
                {
                    public Select_camera3()
                    {
                        this.command = 0x3D;
                        this.idaction = 13;
                        this.name = "Select camera 3";
                    }
                }
            }
            public class Camera4 : Model.GAME.SECTION.TASK
            {
                public Camera4()
                {
                    this.idtask = 14;
                    this.Event = new Camera4_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Select_camera4());
                }
                public class Camera4_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Camera4_event()
                    {
                        this.idevent = 14;
                        this.name = "Camera 4";
                    }
                }
                public class Select_camera4 : Model.GAME.SECTION.TASK.ACTION
                {
                    public Select_camera4()
                    {
                        this.command = 0x3E;
                        this.idaction = 14;
                        this.name = "Select camera 4";
                    }
                }
            }
            public class SwitchOffAll : Model.GAME.SECTION.DIRECT_ACTION
            {
                public SwitchOffAll()
                {
                    this.command = 0x3F;
                    this.idaction = 15;
                    this.name = "Switch off all";
                }
            }
        }
        public class Lightning : Model.GAME.SECTION
        {
            public Lightning()
            {
                this.idsection = 8;
                this.name = "Lightning";
                this.Actions = new List<DIRECT_ACTION>();
                this.Tasks = new List<TASK>();
                this.Tasks.Add(new Light_room1());
                this.Tasks.Add(new Light_room2());
                this.Tasks.Add(new Light_room3());
                this.Tasks.Add(new Light_room4());
            }
            public class Light_room1 : Model.GAME.SECTION.TASK
            {
                public Light_room1()
                {
                    this.idtask = 15;
                    this.Event = new Light_room1_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Turn_light1());
                }
                public class Light_room1_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Light_room1_event()
                    {
                        this.idevent = 15;
                        this.name = "Light room 1";
                    }
                }
                public class Turn_light1 : Model.GAME.SECTION.TASK.ACTION
                {
                    public Turn_light1()
                    {
                        this.command = 0x40;
                        this.idaction = 16;
                        this.name = "Turn light 1";
                    }
                }
            }
            public class Light_room2 : Model.GAME.SECTION.TASK
            {
                public Light_room2()
                {
                    this.idtask = 16;
                    this.Event = new Light_room2_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new TurnLight2());
                }
                public class Light_room2_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Light_room2_event()
                    {
                        this.idevent = 16;
                        this.name = "Light room 2";
                    }
                }
                public class TurnLight2 : Model.GAME.SECTION.TASK.ACTION
                {
                    public TurnLight2()
                    {
                        this.command = 0x41;
                        this.idaction = 17;
                        this.name = "Turn light 2";
                    }
                }
            }
            public class Light_room3 : Model.GAME.SECTION.TASK
            {
                public Light_room3()
                {
                    this.idtask = 17;
                    this.Event = new Light_room3_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new TurnLight3());
                }
                public class Light_room3_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Light_room3_event()
                    {
                        this.idevent = 17;
                        this.name = "Light room 3";
                    }
                }
                public class TurnLight3 : Model.GAME.SECTION.TASK.ACTION
                {
                    public TurnLight3()
                    {
                        this.command = 0x42;
                        this.idaction = 18;
                        this.name = "Turn light 3";
                    }
                }
            }
            public class Light_room4 : Model.GAME.SECTION.TASK
            {
                public Light_room4()
                {
                    this.idtask = 18;
                    this.Event = new Light_room4_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Turn_light4());
                }
                public class Light_room4_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Light_room4_event()
                    {
                        this.idevent = 18;
                        this.name = "Light room 4";
                    }
                }
                public class Turn_light4 : Model.GAME.SECTION.TASK.ACTION
                {
                    public Turn_light4()
                    {
                        this.command = 0x43;
                        this.idaction = 19;
                        this.name = "Turn light 4";
                    }
                }
            }
        }
        public class Lightning_rooms : Model.GAME.SECTION
        {
            public Lightning_rooms()
            {
                this.Actions = new List<DIRECT_ACTION>();
                this.idsection = 9;
                this.name = "Lightning rooms";
                this.Tasks = new List<TASK>();
                this.Actions.Add(new Light_off());
                this.Actions.Add(new Light_on());
            }
            public class Light_on : Model.GAME.SECTION.DIRECT_ACTION
            {
                public Light_on()
                {
                    this.command = 0x44;
                    this.idaction = 20;
                    this.name = "Light on";
                }
            }
            public class Light_off : Model.GAME.SECTION.DIRECT_ACTION
            {
                public Light_off()
                {
                    this.command = 0x45;
                    this.idaction = 21;
                    this.name = "Light off";
                }
            }
        }
        public class Test : Model.GAME.SECTION
        {
            public Test()
            {
                this.Actions = new List<DIRECT_ACTION>();
                this.idsection = 10;
                this.name = "Test";
                this.Tasks = new List<TASK>();
                this.Actions.Add(new TestLights());
            }
            public class TestLights : Model.GAME.SECTION.DIRECT_ACTION
            {
                public TestLights()
                {
                    this.cmdTests = new byte[2] { 0x44, 0x45 };
                    this.idaction = 22;
                    this.name = "Test";
                    this.testFlag = true;
                }
            }
        }
    }
    public class TimeMachine : Model.GAME
    {
        public TimeMachine()
        {
            this.idgame = 3;
            this.mcadr = 4;
            this.name = "Traped in time";
            this.shortName = "TimeMachine";
            this.Sections = new List<SECTION>();
            this.Sections.Add(new Wall_calendar());
            this.Sections.Add(new Clock());
            this.Sections.Add(new Exit_door());
            this.Sections.Add(new Time_machine());
            this.Sections.Add(new Picture_RFID());
            this.Sections.Add(new Door_time_machine());
            this.Sections.Add(new Calendar_poster());
            this.Sections.Add(new Lightning_rooms());
        }
        public class Wall_calendar : Model.GAME.SECTION
        {
            public Wall_calendar()
            {
                this.idsection = 1;
                this.name = "Wall calendar";
                this.Actions = new List<DIRECT_ACTION>();
                this.Tasks = new List<TASK>();
                this.Tasks.Add(new Wall_calendar_task());
            }
            public class Wall_calendar_task : Model.GAME.SECTION.TASK
            {
                public Wall_calendar_task()
                {
                    this.idtask = 1;
                    this.Event = new Wall_calendar_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Open_calendar_cache());
                }
                public class Wall_calendar_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Wall_calendar_event()
                    {
                        this.idevent = 1;
                        this.name = "Wall calendar";
                    }
                }
                public class Open_calendar_cache : Model.GAME.SECTION.TASK.ACTION
                {
                    public Open_calendar_cache()
                    {
                        this.command = 0x31;
                        this.idaction = 1;
                        this.name = "Open cal. cache";
                    }
                }
            }
        }
        public class Clock : Model.GAME.SECTION
        {
            public Clock()
            {
                this.idsection = 2;
                this.name = "Clock";
                this.Actions = new List<DIRECT_ACTION>();
                this.Tasks = new List<TASK>();
                this.Tasks.Add(new Clock_task());
            }
            public class Clock_task : Model.GAME.SECTION.TASK
            {
                public Clock_task()
                {
                    this.Event = new Clock_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Open_clock_cache());
                    this.idtask = 2;
                }
                public class Clock_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Clock_event()
                    {
                        this.idevent = 2;
                        this.name = "Clock";
                    }
                }
                public class Open_clock_cache : Model.GAME.SECTION.TASK.ACTION
                {
                    public Open_clock_cache()
                    {
                        this.command = 0x32;
                        this.idaction = 2;
                        this.name = "Open clk cache";
                    }
                }
            }
        }
        public class Exit_door : Model.GAME.SECTION
        {
            public Exit_door()
            {
                this.name = "Exit door";
                this.Tasks = new List<TASK>();
                this.idsection = 3;
                this.Actions = new List<DIRECT_ACTION>();
                this.Tasks.Add(new Btn_panel());
            }
            public class Btn_panel : Model.GAME.SECTION.TASK
            {
                public Btn_panel()
                {
                    this.idtask = 3;
                    this.Event = new Btn_panel_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Open_exit());
                }
                public class Btn_panel_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Btn_panel_event()
                    {
                        this.idevent = 3;
                        this.name = "Btn panel";
                    }
                }
                public class Open_exit : Model.GAME.SECTION.TASK.ACTION
                {
                    public Open_exit()
                    {
                        this.command = 0x33;
                        this.idaction = 3;
                        this.name = "Open exit door";
                    }
                }
            }
        }
        public class Time_machine : Model.GAME.SECTION
        {
            public Time_machine()
            {
                this.idsection = 4;
                this.name = "Time machine";
                this.Tasks = new List<TASK>();
                this.Tasks.Add(new Perfo_RFID());
                this.Tasks.Add(new Date());
                this.Actions = new List<DIRECT_ACTION>();
                this.Actions.Add(new Switch_on_led());
                this.Actions.Add(new Switch_off_led());
            }
            public class Perfo_RFID : Model.GAME.SECTION.TASK
            {
                public Perfo_RFID()
                {
                    this.idtask = 4;
                    this.Event = new Perfo_RFID_event();
                    this.Actions = new List<ACTION>();
                }
                public class Perfo_RFID_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Perfo_RFID_event()
                    {
                        this.idevent = 4;
                        this.name = "Perfo RFID";
                    }
                }
            }
            public class Date : Model.GAME.SECTION.TASK
            {
                public Date()
                {
                    this.idtask = 5;
                    this.Event = new Date_event();
                    this.Actions = new List<ACTION>();
                }
                public class Date_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Date_event()
                    {
                        this.idevent = 5;
                        this.name = "Date";
                    }
                }
            }
            public class Switch_on_led : Model.GAME.SECTION.DIRECT_ACTION
            {
                public Switch_on_led()
                {
                    this.command = 0x34;
                    this.idaction = 4;
                    this.name = "Switch on led";
                }
            }
            public class Switch_off_led : Model.GAME.SECTION.DIRECT_ACTION
            {
                public Switch_off_led()
                {
                    this.command = 0x35;
                    this.idaction = 5;
                    this.name = "Switch off led";
                }
            }
        }
        public class Picture_RFID : Model.GAME.SECTION
        {
            public Picture_RFID()
            {
                this.idsection = 5;
                this.name = "Picture";
                this.Actions = new List<DIRECT_ACTION>();
                this.Tasks = new List<TASK>();
                this.Tasks.Add(new Picture_rfid_task());
            }
            public class Picture_rfid_task : Model.GAME.SECTION.TASK
            {
                public Picture_rfid_task()
                {
                    this.idtask = 6;
                    this.Event = new Picture_rfid_event();
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Block_Picture());
                    this.Actions.Add(new Disblock_Picture());
                }
                public class Picture_rfid_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Picture_rfid_event()
                    {
                        this.idevent = 6;
                        this.name = "Picture RFID";
                    }
                }
                public class Block_Picture : Model.GAME.SECTION.TASK.ACTION
                {
                    public Block_Picture()
                    {
                        this.command = 0x36;
                        this.idaction = 6;
                        this.name = "Block Picture";
                    }
                }
                public class Disblock_Picture : Model.GAME.SECTION.TASK.ACTION
                {
                    public Disblock_Picture()
                    {
                        this.command = 0x37;
                        this.idaction = 7;
                        this.name = "Disblock picture";
                    }
                }
            }
        }
        public class Door_time_machine : Model.GAME.SECTION
        {
            public Door_time_machine()
            {
                this.name = "Door time machine";
                this.Tasks = new List<TASK>();
                this.Tasks.Add(new Open_sensor());
                this.Tasks.Add(new Close_sensor());
                this.Tasks.Add(new Ray_sensor());
                this.Actions = new List<DIRECT_ACTION>();
                this.Actions.Add(new Open_door_Tm());
                this.Actions.Add(new Close_door_TM());
                this.idsection = 6;
            }
            public class Open_sensor : Model.GAME.SECTION.TASK
            {
                public Open_sensor()
                {
                    this.idtask = 7;
                    this.Event = new Open_sensor_event();
                    this.Actions = new List<ACTION>();
                }
                public class Open_sensor_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Open_sensor_event()
                    {
                        this.idevent = 7;
                        this.name = "Open sensor";
                    }
                }
            }
            public class Close_sensor : Model.GAME.SECTION.TASK
            {
                public Close_sensor()
                {
                    this.Actions = new List<ACTION>();
                    this.Event = new Close_sensor_event();
                    this.idtask = 8;
                }
                public class Close_sensor_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Close_sensor_event()
                    {
                        this.idevent = 8;
                        this.name = "Close sensor";
                    }
                }
            }
            public class Ray_sensor : Model.GAME.SECTION.TASK
            {
                public Ray_sensor()
                {
                    this.Actions = new List<ACTION>();
                    this.Event = new Ray_sensor_event();
                    this.idtask = 9;
                }
                public class Ray_sensor_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Ray_sensor_event()
                    {
                        this.idevent = 9;
                        this.name = "Ray sensor";
                    }
                }
            }
            public class Open_door_Tm : Model.GAME.SECTION.DIRECT_ACTION
            {
                public Open_door_Tm()
                {
                    this.command = 0x38;
                    this.idaction = 8;
                    this.name = "Open door TM";
                }
            }
            public class Close_door_TM : Model.GAME.SECTION.DIRECT_ACTION
            {
                public Close_door_TM()
                {
                    this.command = 0x39;
                    this.idaction = 9;
                    this.name = "Close door TM";
                }
            }
        }
        public class Calendar_poster : Model.GAME.SECTION
        {
            public Calendar_poster()
            {
                this.Actions = new List<DIRECT_ACTION>();
                this.idsection = 7;
                this.name = "Calendar poster";
                this.Tasks = new List<TASK>();
                this.Tasks.Add(new Calendar_poster_task());
            }
            public class Calendar_poster_task : Model.GAME.SECTION.TASK
            {
                public Calendar_poster_task()
                {
                    this.Actions = new List<ACTION>();
                    this.Actions.Add(new Open_box_poster());
                    this.Event = new Calendar_poster_event();
                    this.idtask = 10;
                }
                public class Calendar_poster_event : Model.GAME.SECTION.TASK.EVENT
                {
                    public Calendar_poster_event()
                    {
                        this.idevent = 10;
                        this.name = "Calendar poster";
                    }
                }
                public class Open_box_poster : Model.GAME.SECTION.TASK.ACTION
                {
                    public Open_box_poster()
                    {
                        this.command = 0x3A;
                        this.idaction = 10;
                        this.name = "Open box poster";
                    }
                }
            }
        }
        public class Lightning_rooms : Model.GAME.SECTION
        {
            public Lightning_rooms()
            {
                this.Actions = new List<DIRECT_ACTION>();
                this.idsection = 8;
                this.name = "Lightning rooms";
                this.Tasks = new List<TASK>();
                this.Actions.Add(new Light_Off());
                this.Actions.Add(new Light_On());
            }
            public class Light_On : Model.GAME.SECTION.DIRECT_ACTION
            {
                public Light_On()
                {
                    this.command = 0x3B;
                    this.idaction = 11;
                    this.name = "Light on";
                }
            }
            public class Light_Off : Model.GAME.SECTION.DIRECT_ACTION
            {
                public Light_Off()
                {
                    this.command = 0x3C;
                    this.idaction = 12;
                    this.name = "Light off";
                }
            }
        }
    }
    public class DefaultGames:Model
    {
        public DefaultGames()
        {
            this.Controls = new CONTROL();
            this.Games = new List<GAME>();
            this.Games.Add(new DraculaGame());
            this.Games.Add(new StalkerGame());
            this.Games.Add(new TimeMachine());
        }
    }

}
