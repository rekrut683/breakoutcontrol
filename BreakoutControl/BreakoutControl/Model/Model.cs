﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BreakoutControl
{
    public class Model
    {
        public CONTROL_GAME Controls = new CONTROL_GAME();
        public List<GAME> Games = new List<GAME>();
        public class CONTROL_GAME
        {
            public string name;
            public List<CONTROL_SECTION> Sections = new List<CONTROL_SECTION>();
            public class CONTROL_SECTION
            {
                public string name;
                public List<CONTROL_ACTION> Actions = new List<CONTROL_ACTION>();
                public class CONTROL_ACTION
                {
                    public int id;
                    public string name;
                    public bool pollFlag;
                    public bool changeable;
                    public string changeName;
                    //public int[] command;
                    public string commandname;
                    public string changecommandname;
                }
            }
        }       
        public class GAME
        {
            public int idgame;
            public string name;
            public int mcadr;
            public string shortName;
            public List<SECTION> Sections = new List<SECTION>();
            public class SECTION
            {
                public int idsection;
                public string name;
                public List<TASK> Tasks = new List<TASK>();
                public List<DIRECT_ACTION> Actions = new List<DIRECT_ACTION>();
                public class TASK
                {
                    public int idtask;
                    public EVENT Event;
                    public List<ACTION> Actions = new List<ACTION>();
                    public class EVENT
                    {
                        public int idevent;
                        public string name;
                    }
                    public class ACTION
                    {
                        public int idaction;
                        public string name;
                        public byte command;
                    }
                }
                public class DIRECT_ACTION
                {
                    public int idaction;
                    public string name;
                    public byte command;
                    public bool testFlag;
                    public byte[] cmdTests;
                }
            }
        }
    }
}
