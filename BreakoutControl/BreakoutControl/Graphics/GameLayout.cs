﻿using BreakoutControl.Controllers;
using BreakoutControl.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BreakoutControl.Graphics
{
    public class GameLayout : Panel
    {
        public GameLayout() : base()
        {
            this.Width = GraphicController.tPanel.Width;
            this.AutoScroll = false;
        }
    }
}
