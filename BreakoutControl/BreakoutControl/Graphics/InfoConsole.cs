﻿using BreakoutControl.Controllers;
using BreakoutControl.Settings;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BreakoutControl.Graphics
{
    public class InfoConsole : GroupBox
    {
        private LstConsole list;
        private Dictionary<int, ItemInfo> listMessage;
        private int maxItems = SysSettingsController.sizeinfoconsole;
        private class ItemInfo
        {
            public int id;
            public string item;
            public Brush brush;
            public ItemInfo(int id, string msg)
            {
                this.id = id;
                this.item = msg;
                this.brush = Brushes.Red;
            }
        }
        public InfoConsole(int top) : base()
        {
            this.listMessage = new Dictionary<int, ItemInfo>();
            this.Text = GraphicController.iConsole.Text;
            this.Width = GraphicController.tPanel.Width; ;
            this.Height = GraphicController.iConsole.Height; ;
            this.Top = top + 2 * GraphicController.cPanel.Indent; ;
            this.Left = GraphicController.tasks.IndentLeft; ;
            this.list = new LstConsole(this.Height);
            this.list.DrawItem += drawItems;
            this.list.SelectedIndexChanged += onSelect;
            this.list.MouseDown += mouseDown;
            this.Controls.Add(list);
        }
        public void add(string msg)
        {
            if (this.listMessage.Count + 1 > this.maxItems) this.listMessage.Clear();
            int id = (listMessage.Count == 0) ? 0 : listMessage.Values.Last().id + 1;
            ItemInfo ii = new ItemInfo(id, msg);
            this.listMessage.Add(id, ii);
            redraw(this.getStrings());
        }
        private void redraw(string[] msgs)
        {
            if (this.InvokeRequired)
                this.Invoke(new AddToInfoListString(redraw), new object[] { msgs });
            else
            {
                this.list.Items.Clear();
                foreach (string m in msgs) this.list.Items.Add(m);
                this.list.SelectedIndex = this.list.Items.Count - 1;
                this.list.SelectedIndex = -1;
            }
        }
        private void drawItems(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();
            if (e.Index == -1) return;
            e.Graphics.DrawString(list.Items[e.Index].ToString(),
                                  e.Font,
                                  listMessage[e.Index].brush,
                                  e.Bounds,
                                  StringFormat.GenericDefault);

            e.DrawFocusRectangle();
        }
        private void onSelect(object sender, EventArgs e)
        {
            list.SelectedItem = null;
        }
        private void mouseDown(object sender, MouseEventArgs e)
        {
            foreach (ItemInfo ii in listMessage.Values) ii.brush = Brushes.Black;
            redraw(this.getStrings());
        }
        private string[] getStrings()
        {
            List<string> ss = new List<string>();
            foreach (ItemInfo ii in listMessage.Values) ss.Add(ii.item);
            return ss.ToArray();
        }
        public class LstConsole : ListBox
        {
            public LstConsole(int top) : base()
            {
                this.Top = GraphicController.cPanel.Indent;
                this.Height = GraphicController.iConsole.Height - GraphicController.iConsole.HeightIndent;
                this.Width = GraphicController.cPanel.Width - GraphicController.iConsole.WidthIndent;
                this.Left = GraphicController.iConsole.LeftPosition;
                this.DrawMode = DrawMode.OwnerDrawFixed;
            }
        }
    }
}
