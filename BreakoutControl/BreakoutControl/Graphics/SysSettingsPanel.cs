﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BreakoutControl.Graphics
{
    public class SysSettingsPanel : GroupBox
    {
        CbBox cbox;
        TxtBox tBox;
        public SysSettingsPanel(string name, string type, ref int top) : base()
        {
            this.Width = 200;
            this.Height = 50;
            this.Top = top + 10;
            this.Left = 5;
            this.Text = name;
            switch (type)
            {
                case "TXT":
                    tBox = new TxtBox();
                    this.Controls.Add(tBox);
                    break;
                case "CMB":
                    cbox = new CbBox();
                    this.Controls.Add(cbox);
                    break;
            }
            top = this.Bottom;
        }
        public void SetElement(string value)
        {
            if (this.cbox != null || this.tBox != null)
            {
                if (this.InvokeRequired)
                    this.Invoke(new SetElementSettings(SetElement), new object[] { value });
                else
                {
                    if (tBox != null) tBox.Text = value;
                    if (cbox != null) cbox.Text = value;
                }
            }
        }
        public void FillCombo(string[] ss)
        {
            if (this.cbox != null)
            {
                if (this.InvokeRequired)
                    this.Invoke(new FillCombobox(FillCombo), new object[] { ss });
                else
                {
                    foreach (string s in ss) this.cbox.Items.Add(s);
                }
            }
        }
        private class TxtBox : TextBox
        {
            public TxtBox() : base()
            {
                this.Width = 180;
                this.Height = 30;
                this.Top = 20;
                this.Left = 10;
            }
        }
        private class CbBox : ComboBox
        {
            public CbBox() : base()
            {
                this.Width = 180;
                this.Height = 30;
                this.Top = 20;
                this.Left = 10;
            }
        }
    }
}
