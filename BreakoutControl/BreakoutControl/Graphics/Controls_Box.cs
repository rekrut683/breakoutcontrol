﻿using BreakoutControl.Controllers;
using BreakoutControl.Settings;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BreakoutControl.Graphics
{
    public class ControlsBox : GroupBox
    {
        private int gameid;
        private CheckConsole check;
        private TimeConsole time;
        private ProgressConsole progress;
        public CheckConsole Check
        {
            get { return this.check; }
        }
        public TimeConsole Time
        {
            get { return this.time; }
        }
        public ProgressConsole Progress
        {
            get { return this.progress; }
        }
        public ControlsBox(Model model, int gid, ref int bottomControlPanel) : base()
        {
            this.Width = GraphicController.cPanel.Width;
            this.Height = GraphicController.cPanel.Height;
            this.Top = GraphicController.tabs.HeightTaskLayout -GraphicController.cPanel.InnerIndent ;
            this.gameid = gid;
            this.Text = model.Controls.name;
            this.Name = model.Controls.name;
            int left = 0;
            foreach (Model.CONTROL_GAME.CONTROL_SECTION sect in model.Controls.Sections)
            {
                ControlItem ci = new ControlItem(ref left, sect.name, sect.Actions, this.gameid);
                this.Controls.Add(ci);
            }
            this.time = new TimeConsole(GraphicController.cPanel.Width, this.gameid);
            this.Controls.Add(this.time);
            this.check = new CheckConsole(this.time.Left, this.gameid);
            this.Controls.Add(this.check);
            this.progress = new ProgressConsole(this.check.Left, this.gameid);
            this.Controls.Add(this.progress);
            bottomControlPanel = this.Bottom;   
        }
        private class ControlItem : GroupBox
        {
            private int gameid;
            public ControlItem(ref int pos, string nm, List<Model.CONTROL_GAME.CONTROL_SECTION.CONTROL_ACTION> actions, int gid) : base()
            {
                this.Text = nm;
                this.gameid = gid;
                this.Height = GraphicController.cPanel.HeightControlItem;
                this.Top = GraphicController.cPanel.TopControlItem;
                this.Left = pos + GraphicController.cPanel.IndentLeftControlItem;
                int left = 0;
                this.Width = GraphicController.cPanel.InnerIndent;
                foreach (Model.CONTROL_GAME.CONTROL_SECTION.CONTROL_ACTION acts in actions)
                {
                    ButtonControls btns = new ButtonControls(acts.name,
                                                             gameid,
                                                             acts.id, 
                                                             acts.commandname, 
                                                             acts.changeable, 
                                                             acts.changeName, 
                                                             acts.changecommandname);
                    if(btns.Change) Instances.Games[gameid].CtrlBtns.Add(btns);
                    btns.Left = left + GraphicController.cPanel.InnerIndent;
                    btns.Top = 20;
                    btns.Id = acts.id;
                    btns.Click += btnClicks;
                    left += btns.Width;
                    this.Controls.Add(btns);
                    this.Width += btns.Width;
                }
                this.Width += GraphicController.cPanel.InnerIndent;
                pos = this.Right;
            }
            private void btnClicks(object sender, EventArgs e)
            {
                ButtonControls bc = (ButtonControls)sender;
                var id = bc.Id;
                var gid = this.gameid;
                var str = bc.CurrentAction;
                GameInstance gi = Instances.Games[gid];
                gi.ActionProcess.InitAction(str);
                //if (bc.Change) Driver.setCallback(bc.onChangeFn);
                gi.ActionFForm(str); 
            }
        }
        public class ButtonControls : Button
        {
            private int gid;
            private int id;
            private string name = "";
            private string action;
            private bool change = false;
            private string changename = "";
            private string changeaction = "";
            private string currentaction = "";
            public int Id
            {
                get { return this.id; }
                set { this.id = value; }
            }
            public string CurrentAction
            {
                get { return this.currentaction; }
            }
            public bool Change
            {
                get { return this.change; }
            }
            public ButtonControls(string name, 
                                  int gid,
                                  int uid, 
                                  string action, 
                                  bool change, 
                                  string cname, 
                                  string caction) : base()
            {
                this.gid = gid;
                this.name = name;
                this.id = uid;
                this.action = action;
                this.change = change;
                this.changename = cname;
                this.changeaction = caction;
                this.currentaction = action;
                this.Text = this.name;
            }
            public void onChangeFn(bool state)
            {
                if (this.InvokeRequired)
                    this.Invoke(new ChangeNameButton(onChangeFn), new object[] { state });
                else
                {
                    switch (state)
                    {
                        case true:
                            this.Text = changename;
                            this.currentaction = changeaction;
                            break;
                        case false:
                            int g = this.gid;
                            this.Text = name;
                            this.currentaction = action;
                            break;
                    }
                }
            }
        }
        public class CheckConsole : GroupBox
        {
            private int gameid;
            private CheckLed cl;
            public CheckConsole(int left, int gameid) : base()
            {
                this.Text = GraphicController.checkConsole.TextLabel;
                this.gameid = gameid;
                this.Top = GraphicController.checkConsole.Top;
                this.Width = GraphicController.checkConsole.Width;
                this.Left = left - this.Width - GraphicController.cPanel.InnerIndent;
                this.Height = GraphicController.checkConsole.Height;
                this.cl = new CheckLed();
                this.Controls.Add(cl);
            }
            public void changeStateCheck(int value) { this.cl.changeColor(value); }
            private class CheckLed : PictureBox
            {
                public CheckLed() : base()
                {
                    this.BackColor = SystemColors.ButtonFace;
                    this.SizeMode = PictureBoxSizeMode.StretchImage;
                    this.BackgroundImageLayout = ImageLayout.Center;
                    this.Size = new Size(GraphicController.checkConsole.LedRadius, GraphicController.checkConsole.LedRadius);
                    this.Image = Config.unknownLed;
                    GraphUtils.ResizeImage(this.Image, GraphicController.checkConsole.LedRadius, GraphicController.checkConsole.LedRadius);
                    this.Top = GraphicController.checkConsole.TopLed;
                    this.Left = GraphicController.checkConsole.LeftLed;
                }
                public void changeColor(int value)
                {
                    if (this.InvokeRequired)
                        this.Invoke(new ChangeCheckConsole(changeColor), new object[] { value });
                    else
                    {
                        switch (value)
                        {
                            case (int)GameInstance.MC_STATE.UNKNOWN: this.Image = Config.unknownLed; break;
                            case (int)GameInstance.MC_STATE.OFFLINE: this.Image = Config.offlineLed; break;
                            case (int)GameInstance.MC_STATE.PLAYED: this.Image = Config.playedLed; break;
                            case (int)GameInstance.MC_STATE.STOPPED: this.Image = Config.stoppedLed; break;
                        }
                    }
                }
            }
        }
        public class ProgressConsole : GroupBox
        {
            private int gameid;
            private pBar bar;
            private int pos = 0;
            public ProgressConsole(int left, int gameid)
            {
                this.Text = GraphicController.progress.TextLabel;
                this.gameid = gameid;
                this.Top = GraphicController.progress.Top;
                this.Width = GraphicController.progress.Width;
                this.Left = left - this.Width - GraphicController.cPanel.InnerIndent;
                this.Height = GraphicController.progress.Height;
                bar = new pBar();
                bar.Height = GraphicController.progress.ProgressHeight;
                bar.Width = GraphicController.progress.ProgressWidth;
                bar.Top = GraphicController.progress.TopProgress;
                bar.Left = GraphicController.progress.LeftProgress;
                this.Controls.Add(bar);
            }
            private class pBar : ProgressBar
            {
                public pBar() : base()
                {
                    this.Minimum = 0;
                    this.Maximum = 100;
                    this.Step = 5;
                }
            }
            public void MoveBar()
            {
                if (this.InvokeRequired)
                    this.Invoke(new MoveProgressBar(MoveBar), new object[] { });
                else
                {
                    if (this.bar.Value == this.bar.Maximum)
                    {
                        this.bar.PerformStep();
                        this.bar.Value = 0;
                        return;
                    }
                    this.bar.PerformStep();                   
                }
            }
        }
        public class TimeConsole : GroupBox
        {
            private int gameid;
            private PartTime minLabel;
            private PartTime secLabel;
            private int leftPosition = GraphicController.timeConsole.LeftPos;
            public TimeConsole(int left, int gid) : base()
            {
                this.Text = GraphicController.timeConsole.TextLabel;
                this.gameid = gid;
                this.Top = GraphicController.timeConsole.TopTimeConsole;
                this.Width = GraphicController.timeConsole.WidthTimeConsole;
                this.Left = left - this.Width - GraphicController.cPanel.InnerIndent;
                this.Height = GraphicController.timeConsole.HeightTimeConsole;
                this.minLabel = new PartTime(GraphicController.timeConsole.MinuteInitLabel, GraphicController.timeConsole.WidthPartTime, ref leftPosition);
                this.Controls.Add(this.minLabel);
                PartTime ptSeparator = new PartTime(":", GraphicController.timeConsole.WidthSeparator, ref leftPosition);
                this.Controls.Add(ptSeparator);
                this.secLabel = new PartTime(GraphicController.timeConsole.SecondsInitLabel, GraphicController.timeConsole.WidthPartTime, ref leftPosition);
                this.Controls.Add(this.secLabel);
            }
            public void Default()
            {
                this.setTimes(Convert.ToInt32(GraphicController.timeConsole.MinuteInitLabel), 
                              Convert.ToInt32(GraphicController.timeConsole.SecondsInitLabel));
            }
            public void setTimes(int min, int secs)
            {
                if (this.InvokeRequired)
                    this.Invoke(new SetTimes(setTimes), new object[] { min, secs });
                else
                {
                    string mins = min.ToString();
                    string seconds = secs.ToString();
                    if (mins.Length == 1) mins = "0" + mins;
                    if (seconds.Length == 1) seconds = "0" + seconds;
                    this.minLabel.Lbl.Text = mins;
                    this.secLabel.Lbl.Text = seconds;
                }
            }
            private class PartTime : Panel
            {
                private TimeLabel lbl;
                public TimeLabel Lbl
                {
                    get { return this.lbl; }
                    set { this.lbl = value; }
                }
                public PartTime(string value, int wid, ref int lft) : base()
                {
                    this.Left = lft;
                    this.Top = GraphicController.timeConsole.TopPartTime;
                    this.Width = wid;
                    this.Height = GraphicController.timeConsole.HeightPartTime;
                    this.Padding = new Padding(GraphicController.timeConsole.PaddingTime);
                    this.Margin = new Padding(GraphicController.timeConsole.MarginTime);
                    this.AutoSize = false;
                    this.lbl = new TimeLabel(value);
                    lft += this.Width;
                    this.Controls.Add(this.lbl);
                }
            }
            private class TimeLabel : Label
            {
                public TimeLabel(string value) : base()
                {
                    this.Text = value;
                    this.Left = 0;
                    this.Top = 0;
                    this.Font = new Font("Microsoft Sans Serif", GraphicController.timeConsole.FontSize, FontStyle.Bold, GraphicsUnit.Point, ((byte)(204)));
                    this.ForeColor = Color.FromName(GraphicController.timeConsole.Color);
                }
            }
        }
    }
}
