﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BreakoutControl.Graphics
{
    public delegate void ChangeCheckConsole(int value);
    public delegate void ChangeStateConsole(int value, int gameid);
    public delegate void SetTimes(int min, int secs);
    public delegate void AddToInfoList(string msg);
    public delegate void AddToInfoListString(string[] msgs);
    public delegate void ClearInfoList();
    public delegate void ChangeNameButton(bool state);
    public delegate void ChangeStateEventLed(bool state);
    public delegate void FillCombobox(string[] ss);
    public delegate void SetElementSettings(string value);
    public delegate void MoveProgressBar();

    public delegate void OnConsoleReady();
    public delegate void OnStateConsoleReady();
}
