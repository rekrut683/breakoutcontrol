﻿using BreakoutControl.Controllers;
using BreakoutControl.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BreakoutControl.Graphics
{
    /******************Wrapper for action (button for make actions)******************/
    public class ContainerAction : Panel
    {
        /********************CONSTRUCTOR*************************/
        public ContainerAction(string name, int gid, ref int top, byte? cmd, byte[] cmds = null, bool isTest = false) : base()
        {
            //width of container
            this.Width = GraphicController.actions.Width;
            //height of container
            this.Height = GraphicController.actions.Height;
            //left position of container
            this.Left = GraphicController.actions.IndentLeft;
            //top position: nominated top position from FormController plus indent top for container template
            this.Top = top + GraphicController.actions.IndentTop;
            //create button
            BtnAction btn = new BtnAction(name, gid, this.ClientSize.Height, cmd, cmds, isTest);
            //create event click to button action
            btn.Click += btnClick;
            //add button to this container
            this.Controls.Add(btn);
            //return this container height (bottom minus top = height)
            top += this.Bottom - this.Top;
        }

        /*****************HANDLER OF BUTTON CLICK***********************/
        private void btnClick(object sender, EventArgs e)
        {
            BtnAction action = (BtnAction)sender;
            //set action = Execute (execute commands)
            string act = ProtoController.actions.Execute;
            var cmd = action.cmd;
            var gid = action.gameid;

            //get Game Instance
            GameInstance gi = Instances.Games[gid];
            if (action.isTest)
            {
                if (action.isLoop) action.isLoop = false;
                else
                {
                    ObjLoop obj = new ObjLoop(gi, action);
                    action.isLoop = true;
                    Thread th = new Thread(new ParameterizedThreadStart(loopTest));
                    th.IsBackground = true;
                    th.Start(obj);
                }
                return;
            }
            //execute method handler form actions
            gi.ActionFForm(act, cmd);
        }

        public class ObjLoop
        {
            public GameInstance gi;
            public BtnAction action;
            public ObjLoop(GameInstance g, BtnAction act)
            {
                this.gi = g;
                this.action = act;
            }
        }

        private static void loopTest(object obj)
        {
            ObjLoop ob = (ObjLoop)obj;
            int len = ob.action.cmdsTest.Length;
            if (len <= 0) return;
            int cur_pos = 0;
            while (ob.action.isLoop)
            {
                Thread.Sleep(3000);
                byte cmd = ob.action.cmdsTest[cur_pos];
                cur_pos++;
                if (cur_pos > len - 1) cur_pos = 0;
                ob.gi.ActionFForm(ProtoController.actions.Execute, cmd);
            }
        }

        /**************CLASS OF BUTTON**************/
        public class BtnAction : Button
        {
            /*****************PRIVATE FIELDS***************/
            public int gameid;
            public byte cmd;
            protected internal byte[] cmdsTest;
            protected internal bool isTest;
            protected internal bool isLoop;

            /*****************CONSTRUCTOR******************/
            public BtnAction(string n, int gid, int tops, byte? cmd, byte[] cmds, bool isTest) : base()
            {
                //set self properties
                this.Name = n;
                this.Text = n;
                this.gameid = gid;
                this.cmd = cmd.GetValueOrDefault();
                this.cmdsTest = cmds;
                this.isTest = isTest;
                //set left position
                this.Left = GraphicController.btns.Indent;
                //set self width
                this.Width = GraphicController.btns.Width;
                //set top posotion: top position nominated from Container Controller minus this height reduced in 2
                this.Top = (tops - this.Height) / 2;
            }
        }
    }
}
