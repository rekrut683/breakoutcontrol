﻿using BreakoutControl.Settings;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.TabControl;
using BreakoutControl.Graphics;
using BreakoutControl.Controllers;

namespace BreakoutControl.Graphics
{
    public class MainForm : Form
    {
        public static event OnConsoleReady onConsoleReady;
        public static event OnStateConsoleReady onStateConsoleReady;

        public static State_Console StateConsole;
        public static InfoConsole IConsole;
        public static SettingsPage SettingsPg;

        private TabPanel tabPanel;
        private Model model;
        private TabPageCollection pageCollection;
        private int bottomControlPanel;

        /**************CONSTRUCTORS******************************************/
        public MainForm(Model model) : base()
        {
            initForm();
            this.model = model;
            CreateControls();
        }
        /*****************************PRIVATE METHODS****************************************/
        private void initForm()
        {
            this.AutoScaleDimensions = new SizeF(6F, 13F);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.ClientSize = new Size(GraphicController.mForm.Width, GraphicController.mForm.Height);

            //------------ХОТЯТ ЧТОБЫ ФОРМА ИЗМЕНЯЛА РАЗМЕР----------------//
            //this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.FormBorderStyle = FormBorderStyle.Sizable;
            //--------------------------------------------------------------//

            //------------ДОБАВИМ СКРОЛЛ------------------------------------//
            this.AutoScroll = true;
            //--------------------------------------------------------------//

            this.Name = GraphicController.mForm.Name;
            this.Text = GraphicController.mForm.Name;

            //-------------------ХОТЯТ ЧТОБЫ ОКНО УМЕНЬШАЛОСЬ И УВЕЛИЧИВАЛОСЬ-----------------//
            this.MaximizeBox = false;
            this.MaximizeBox = true;
            //--------------------------------------------------------------------------//

            this.ResumeLayout(false);
        }
        private void CreateControls()
        {
            tabPanel = new TabPanel();
            this.Controls.Add(tabPanel);
            pageCollection = new TabPageCollection(this.tabPanel);
            CreateGames();
            CreateSettingsPage();
            pageCollection.Add(SettingsPg);
            Instances.ControlsCommands = ProtoController.FillCommands();
            IConsole = new InfoConsole(this.bottomControlPanel);
            if (onConsoleReady != null) onConsoleReady();
            this.Controls.Add(IConsole);
            StateConsole = new State_Console(tabPanel.Right, model);
            if (onStateConsoleReady != null) onStateConsoleReady();
            this.Controls.Add(StateConsole);
        }
        private void CreateGames()
        {
            foreach (Model.GAME game in this.model.Games)
            {
                int posX = 0;
                int posY = GraphicController.sections.IndentTop;
                int currentHeight = 0;
                TabPagePanel tpp = new TabPagePanel(game.name);
                GameLayout gl = new GameLayout();
                GameInstance gi = new GameInstance(game.idgame, game.mcadr, game.name);
                List<GameTasks> gameTasks = new List<GameTasks>();
                Instances.Games.Add(gi.IdGame, gi);
                foreach (Model.GAME.SECTION sect in game.Sections)
                {
                    TaskControl taskControl = new TaskControl(sect.name, gi.IdGame, sect.Tasks, sect.Actions);
                    foreach (ContainerTask cntTask in taskControl.Tasks) gameTasks.Add(new GameTasks(cntTask, gi.IdGame));
                    currentHeight = posY + GraphicController.sections.IndentTop;
                    if (currentHeight + taskControl.Height >= GraphicController.tabs.HeightTaskLayout)
                    {
                        posY = GraphicController.sections.IndentTop;
                        posX += GraphicController.sections.Width + GraphicController.sections.IndentLeft;
                        currentHeight = 0;
                    }
                    taskControl.Top = posY;
                    taskControl.Left = posX;
                    gl.Controls.Add(taskControl);
                    posY = taskControl.Bottom + GraphicController.sections.IndentTop;
                    currentHeight = posY;
                }
                gi.TaskList = gameTasks;
                ControlsBox cb = new ControlsBox(model, gi.IdGame, ref bottomControlPanel);
                gl.Controls.Add(cb);
                gi.Time = cb.Time;
                gi.CheckConsole = cb.Check;
                gi.Progress = cb.Progress;
                gl.Height = GraphicController.tabs.HeightTaskLayout + GraphicController.cPanel.Height + GraphicController.cPanel.Indent;
                tpp.Controls.Add(gl);
                tpp.AutoScroll = false;
                pageCollection.Add(tpp);
                tabPanel.Height = gl.Bottom;
            }
        }
        private void CreateSettingsPage()
        {
            object res = ControllerUtils.GetInnerFields(SysSettingsController.syssettings);
            Dictionary<string, object> dct = (Dictionary<string, object>)res;
            SettingsPg = new SettingsPage();
            int top = 0;
            foreach (Dictionary<string, object> d in dct.Values)
            {
                string name = "";
                string type = "";
                string value = "";
                foreach (KeyValuePair<string, object> kp in d)
                {
                    if (kp.Key == "name") name = kp.Value.ToString();
                    if (kp.Key == "type") type = kp.Value.ToString();
                    if (kp.Key == "defaultvalue") value = kp.Value.ToString();
                }
                SysSettingsPanel sspanel = new SysSettingsPanel(name, type, ref top);
                if (name.Contains("SerialPort")) sspanel.FillCombo(Driver.GetPorts());
                if (name.Contains("BaudRate")) sspanel.FillCombo(Driver.GetBauds());
                sspanel.SetElement(value);
                SettingsPg.Controls.Add(sspanel);
            }
        }
        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            base.OnFormClosed(e);
            Driver.IsLoop = false; 
        }

    }
}
