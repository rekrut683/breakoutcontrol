﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BreakoutControl.Settings;
using BreakoutControl.Controllers;

namespace BreakoutControl.Graphics
{
    public class TabPanel : TabControl
    {
        public TabPanel() : base()
        {
            this.Padding = new System.Drawing.Point(GraphicController.tPanel.PaddingX,
                                                    GraphicController.tPanel.PaddingY);
            this.Width = GraphicController.tPanel.Width;
            this.Left = GraphicController.tPanel.LeftPosition;
        }
    }
}
