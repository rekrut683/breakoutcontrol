﻿using BreakoutControl.Controllers;
using BreakoutControl.Settings;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BreakoutControl.Graphics
{
    public class State_Console : GroupBox
    {
        private Dictionary<int, LedStateGame> listLeds = new Dictionary<int, LedStateGame>();
        private Dictionary<int, StateLabel> listLabels = new Dictionary<int, StateLabel>();
        public State_Console(int left, Model model) : base()
        {
            this.Width = GraphicController.stateConsole.Width;
            this.Height = GraphicController.tabs.HeightTaskLayout - GraphicController.stateConsole.HeightIndent;
            this.Left = left + GraphicController.cPanel.IndentLeftControlItem;
            this.Top = GraphicController.stateConsole.Top;
            this.Text = GraphicController.stateConsole.Text;
            int beginTop = GraphicController.stateConsole.BeginTop;
            foreach (Model.GAME gms in model.Games)
            {
                int sizeH = createStateElement(gms.shortName, beginTop, gms.idgame);
                beginTop += GraphicController.stateConsole.NextIndent + sizeH;

            }
        }
        private class LedStateGame : PictureBox
        {
            public LedStateGame(int top) : base()
            {
                this.BackColor = SystemColors.ButtonFace;
                this.SizeMode = PictureBoxSizeMode.StretchImage;
                this.BackgroundImageLayout = ImageLayout.Center;
                this.Size = new Size(GraphicController.stateConsole.LedRadius, GraphicController.stateConsole.LedRadius);
                this.Image = Config.unknownLed;
                GraphUtils.ResizeImage(this.Image, GraphicController.stateConsole.LedRadius, GraphicController.stateConsole.LedRadius);
                this.Left = GraphicController.stateConsole.IndentLedLeft; ;
                this.Top = top;
            }
        }
        private class LabelName : Label
        {
            public LabelName(string name, int top) : base()
            {
                this.Text = name;
                this.Width = GraphicController.stateConsole.WidthLabel; ;
                this.Height = GraphicController.stateConsole.HeightLabel; ;
                this.Left = GraphicController.stateConsole.LeftIndentLabel; ;
                this.Top = top;
                this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
                this.AutoSize = false;
            }
        }
        private class StateLabel : Label
        {
            public StateLabel(int top) : base()
            {
                this.Text = Config.unknownTxt;
                this.Width = GraphicController.stateConsole.WidthStateLabel;
                this.Height = GraphicController.stateConsole.HeightLabel;
                this.Left = GraphicController.stateConsole.IndentStateLabel; ;
                this.Top = top;
            }
        }
        private int createStateElement(string name, int top, int gid)
        {
            LabelName lsName = new LabelName(name, top);
            this.Controls.Add(lsName);
            LedStateGame lsg = new LedStateGame(lsName.Bottom);
            StateLabel lsState = new StateLabel(lsg.Bottom + 5);
            this.listLeds.Add(gid, lsg);
            this.listLabels.Add(gid, lsState);
            this.Controls.Add(lsg);
            this.Controls.Add(lsState);
            return lsName.Height + lsg.Height + GraphicController.stateConsole.IndentStateConsole;
        }
        public void changeState(int value, int gid)
        {
            if (this.InvokeRequired)
                this.Invoke(new ChangeStateConsole(changeState), new object[] { value, gid });
            else
            {
                Image img = null;
                string str = "";
                switch (value)
                {
                    case (int)GameInstance.MC_STATE.UNKNOWN:
                        img = Config.unknownLed;
                        str = Config.unknownTxt;
                        break;
                    case (int)GameInstance.MC_STATE.OFFLINE:
                        img = Config.offlineLed;
                        str = Config.offlineTxt;
                        break;
                    case (int)GameInstance.MC_STATE.PLAYED:
                        img = Config.playedLed;
                        str = Config.playedTxt;
                        break;
                    case (int)GameInstance.MC_STATE.STOPPED:
                        img = Config.stoppedLed;
                        str = Config.stoppedTxt;
                        break;
                }
                this.listLeds[gid].Image = img;
                this.listLabels[gid].Text = str;
            }
        }
    }
}
