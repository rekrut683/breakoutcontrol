﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BreakoutControl.Graphics
{
    public class TabPagePanel : TabPage
    {
        public TabPagePanel(string name) : base()
        {
            this.AutoScroll = false;
            this.Name = name;
            this.Text = name;
        }
    }
}
