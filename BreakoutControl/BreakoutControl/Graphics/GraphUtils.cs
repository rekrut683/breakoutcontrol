﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;


namespace BreakoutControl.Graphics
{
    public static class GraphUtils
    {
        public static Image ResizeImage(Image img, int nWidth, int nHeight)
        {
            Image result = new Bitmap(nWidth, nHeight);
            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage((Image)result))
            {
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(img, 0, 0, nWidth, nHeight);
                g.Dispose();
            }
            return result;
        }
    }
}
