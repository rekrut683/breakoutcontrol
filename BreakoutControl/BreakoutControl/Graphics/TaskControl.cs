﻿using BreakoutControl.Controllers;
using BreakoutControl.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BreakoutControl.Graphics
{
    public class TaskControl : GroupBox
    {
        private int gameid;
        private int nextTopCoord = 0;
        private List<ContainerTask> cntTasks = new List<ContainerTask>();
        public IEnumerable<ContainerTask> Tasks
        {
            get { foreach (ContainerTask tsk in cntTasks) yield return tsk; }
            private set { cntTasks = value.ToList(); }
        }
        public TaskControl(string name, int gid, List<Model.GAME.SECTION.TASK> tasks, List<Model.GAME.SECTION.DIRECT_ACTION> actions) : base()
        {
            this.Width = GraphicController.tasks.Width;
            this.Height = GraphicController.tasks.StartHeight;
            this.Name = name;                                                                       
            this.Text = name;
            this.gameid = gid;                                                                                                                                           
            foreach (Model.GAME.SECTION.TASK tsk in tasks)
            {
                ContainerTask container = new ContainerTask(tsk.Event.idevent, tsk.Event.name, ref this.nextTopCoord);
                this.Controls.Add(container);
                this.cntTasks.Add(container);
                this.Height += GraphicController.tasks.Height;
                if (tsk.Actions.Count != 0)
                {
                    foreach (Model.GAME.SECTION.TASK.ACTION act in tsk.Actions)
                    {
                        ContainerAction actionContainer = new ContainerAction(act.name, this.gameid, ref this.nextTopCoord, act.command);
                        this.Controls.Add(actionContainer);
                        this.Height += GraphicController.actions.Height;
                    }
                }
            }
            foreach (Model.GAME.SECTION.DIRECT_ACTION actt in actions)
            {
                ContainerAction directAction = new ContainerAction(actt.name, this.gameid, ref this.nextTopCoord, actt.command, actt.cmdTests, actt.testFlag);
                this.Controls.Add(directAction);
                this.Height += GraphicController.actions.Height;   
            }
        }
    }
}
