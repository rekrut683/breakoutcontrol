﻿using BreakoutControl.Controllers;
using BreakoutControl.Settings;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BreakoutControl.Graphics
{
    /*****************Wrapper of Task Container********************/
    public class ContainerTask : Panel
    {
        /****************PRIVATE FIELDS********************/
        private int id;
        private string name;
        private LedStateControl led;
        private StateLabelTask state;
        
        /****************PUBLIC PROPS**********************/
        public int Id
        {
            get { return id; }
            private set { id = value; }
        }
        public string NameTask
        {
            get { return name; }
            private set { name = value; }
        }

        /****************CONSTRUCTOR************************/
        public ContainerTask(int id, string name, ref int top) : base()
        {
            this.id = id;
            this.name = name;
            this.Width = GraphicController.events.Width;
            this.Height = GraphicController.tasks.Height;
            this.Top = top + GraphicController.tasks.IndentTop;
            this.Left = GraphicController.tasks.IndentLeft;
            Label taskName = new Label();
            taskName.Text = name;
            this.led = new LedStateControl();
            this.led.Location = new Point(taskName.Location.X, taskName.Location.Y + GraphicController.events.IndentTopEvent);
            this.state = new StateLabelTask();
            this.state.Location = new Point(led.Location.X + GraphicController.events.IndentLedLocationX, led.Location.Y + GraphicController.events.IndentLedLocationY);
            this.Controls.Add(taskName);
            this.Controls.Add(led);
            this.Controls.Add(state);
            top += this.Bottom - this.Top;
        }
        private class LedStateControl : PictureBox
        {
            public LedStateControl() : base()
            {
                this.BackColor = SystemColors.ButtonFace;
                this.SizeMode = PictureBoxSizeMode.StretchImage;
                this.BackgroundImageLayout = ImageLayout.Center;
                this.Size = new Size(GraphicController.events.LedRadius, GraphicController.events.LedRadius);
                this.Image = Config.disactiveTaskLed;
                GraphUtils.ResizeImage(this.Image, GraphicController.events.LedRadius, GraphicController.events.LedRadius);
            }
        }
        private class StateLabelTask : Label
        {
            static string notActive = SettingsManager.getCurrentSettings().Resources.LabelState.NotActive;
            static string active = SettingsManager.getCurrentSettings().Resources.LabelState.Active;
            public StateLabelTask() : base()
            {
                this.Size = new Size(57, 13);
                this.Text = notActive;
            }
        }
        public void ChangeLeds(bool state)
        {
            if (this.InvokeRequired)
                this.Invoke(new ChangeStateEventLed(ChangeLeds), new object[] { state });
            else
            {
                Image img = null;
                string txt = "";
                switch (state)
                {
                    case true:
                        img = Config.activeTaskLed;
                        txt = Config.activeTxt;
                        break;
                    case false:
                        img = Config.disactiveTaskLed;
                        txt = Config.disactiveTxt;
                        break;
                }
                this.led.Image = img;
                this.state.Text = txt;
            }
        }
        public bool getState()
        {
            if (this.led.Image == Config.activeTaskLed) return true;
            return false;
        }
    }
}
