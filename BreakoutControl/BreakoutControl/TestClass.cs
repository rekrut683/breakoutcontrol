﻿using BreakoutControl.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BreakoutControl
{
    public static class TestClass
    {
        private static InfoConsole console = MainForm.IConsole;

        private static byte state = 0x00;
        private static byte Frame = 0x7E;
        private static byte mcId = 0x02;
        private static int counter = 0;
        private static byte flowByte = 0x00;
        public static byte[] GetRequest(byte input)
        {
            return getBytes(input);
        }
        private static byte[] getBytes(byte b)
        {
            if (counter == 200) { counter = 0; flowByte = 0; }
            flowByte += (byte)counter;
            List<byte> bb = new List<byte>();
            byte len = 0;
            byte cByte = 0;
            bb.Add(Frame);
            bb.Add(mcId);
            bb.Add(0x01);
            List<byte> addRange = new List<byte>();
            switch (b)
            {
                case 0x10: len = 6; cByte = 0x20; break;
                case 0x11: len = 6; state = 1; cByte = 0x21; break;
                case 0x12: len = 6; state = 0; cByte = 0x22; break;
                case 0x13: len = 6; state = 0; cByte = 0x23; break;
                case 0x44: len = 6; state = 0; cByte = 0xCC; break;
                case 0x45: len = 6; state = 0; cByte = 0xCC; break;
                case 0x14:
                    len = 13;
                    cByte = 0x24;
                    addRange.Add(0x01);
                    addRange.Add(flowByte);
                    addRange.Add(0x12);
                    addRange.Add(0x12);
                    addRange.Add(0x14);
                    addRange.Add(0x05);
                    addRange.Add(0x43);
                    counter++;
                    break;
                case 0x51: len = 7; addRange.Add(state); cByte = 0x72; break;
            }
            bb.Add(len);
            bb.Add(cByte);
            bb.AddRange(addRange);
            bb.Add(Frame);
            return bb.ToArray();
        }
    }
}
