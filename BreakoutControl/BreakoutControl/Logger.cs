﻿using BreakoutControl.Settings;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BreakoutControl
{
    public class Logger
    {
        static int CountThread = 0;
        static Thread runner;
        static object lockRunner = new object();
        static bool stopRunner = false;
        static int countLogger;
        static StreamWriter writer;
        /*****************SETTINGS***********************/
        static string curLevel = SettingsManager.getCurrentSettings().Log.Level;
        static string pathToLog = SettingsManager.getCurrentSettings().Log.PathToLog;
        static string fileName = SettingsManager.getCurrentSettings().Log.Filename;
        static bool archive = SettingsManager.getCurrentSettings().Log.Archive;
        static bool zip = SettingsManager.getCurrentSettings().Log.Zip;
        static int maxArchive = SettingsManager.getCurrentSettings().Log.MaxArchiveFiles;
        static int cleanupArchiveLimit = SettingsManager.getCurrentSettings().Log.CleanupArchiveLimit;
        /******************PRIVATE**********************/
        string className;
        static string assemblyLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        private static object locker = new object();
        //StreamWriter sw;
        static ConcurrentQueue<LogInfo> messageQueue = new ConcurrentQueue<LogInfo>();
        /*******************IDENTIFICATION LOG********************/
        public class LogInfo
        {
            public string msg;
            public string lvl;
            public string classname;
            public DateTime date;
            public LogInfo(string m, string l, string c, DateTime d)
            {
                this.msg = m;
                this.lvl = l;
                this.classname = c;
                this.date = d;
            }
        }
        /**********************ARCHIVE****************************/
        static string archiveDirectory = "LogArchive";
        static string zipDirectory = "ZipArchive";
        static long sizeFileLog = 0;

        /****************CTOR******************/
        public Logger(string name)
        {
            className = name;
            countLogger++;
            lock (lockRunner)
            {
                if (CountThread > 0) return;
                runner = new Thread(queueController);
                runner.IsBackground = true;
                runner.Start();
                CountThread++;
                saveToFile("Log runner is start", "WARNING", this.GetType().ToString(), DateTime.Now);
            }
        }
        ~Logger()
        {
            countLogger--;
            //if (countLogger == 0) stopRunner = true;
        }
        /***************ARCHIVE*******************/
        private static int getNumberOfFile(string name)
        {
            int posFirstUnderline = name.IndexOf('_');
            string cutName = name.Substring(posFirstUnderline + 1);
            int posSecondUnderline = cutName.IndexOf('_');
            string num = cutName.Substring(0, cutName.Length - 1 -
                                          (cutName.Length - 1 - posSecondUnderline));

            return Convert.ToInt32(num);
        }
        private static void startZip(string dir, long hash)
        {
            try
            {
                if (Directory.Exists(assemblyLocation + pathToLog + zipDirectory) == false)
                    Directory.CreateDirectory(assemblyLocation + pathToLog + zipDirectory);
                string path = assemblyLocation + pathToLog + zipDirectory + "\\archive_" +
                              DateTime.UtcNow.ToString("ddMMyyyyHHmmssfff") + "_" + hash + ".zip";
                ZipFile.CreateFromDirectory(dir, path);
                Directory.Delete(dir, true);
            }
            catch (Exception)
            {
                if (Directory.Exists(dir) == true) Directory.Delete(dir, true);
            }
        }
        private static void archiveControl()
        {
            int numNextFile = 1;
            if (Directory.Exists(assemblyLocation + pathToLog + archiveDirectory) == false)
                Directory.CreateDirectory(assemblyLocation + pathToLog + archiveDirectory);
            DirectoryInfo di = new DirectoryInfo(assemblyLocation + pathToLog + archiveDirectory);
            FileInfo[] filesLog = di.GetFiles();
            if (filesLog.Length == 0) numNextFile = 1;
            if (filesLog.Length == maxArchive)
            {
                if (zip == true)
                {
                    DirectoryInfo tempDir = null;
                    string pathtempDir = null;
                    try
                    {
                        var buffer = new byte[sizeof(Int64)];
                        new Random().NextBytes(buffer);
                        long hash = BitConverter.ToInt64(buffer, 0);
                        pathtempDir = assemblyLocation + pathToLog + "tempZip" + hash.ToString();
                        tempDir = Directory.CreateDirectory(pathtempDir);
                        for (int i = 0; i < filesLog.Length; i++)
                        {
                            File.Move(filesLog[i].FullName, Path.Combine(pathtempDir, filesLog[i].Name));
                        }

                        new Thread(() =>
                        {
                            Thread.CurrentThread.IsBackground = true;
                            startZip(pathtempDir, hash);
                        }).Start();
                    }
                    catch (Exception) { }
                }
                else
                {
                    try
                    {
                        for (int i = 0; i < cleanupArchiveLimit; i++) filesLog[i].Delete();
                        FileInfo[] fInfo = di.GetFiles();
                        for (int i = 0; i < fInfo.Length; i++)
                        {
                            int curNumFile = getNumberOfFile(fInfo[i].Name);
                            string newFileName = fInfo[i].Name.Remove(4, 4).
                                                               Insert(4, (i + 1).ToString().PadLeft(4, '0'));
                            File.Move(fInfo[i].FullName, Path.Combine(fInfo[i].DirectoryName, newFileName));
                        }
                        filesLog = di.GetFiles();
                    }
                    catch (Exception)
                    {
                        foreach (FileInfo fi in di.GetFiles()) fi.Delete();
                    }
                }
            }
            if (filesLog.Length > 0 && filesLog.Length < maxArchive)
            {
                int curNumFile = getNumberOfFile(filesLog[filesLog.Length - 1].Name);
                numNextFile = curNumFile + 1;
            }

            string numNextFileStr = numNextFile.ToString().PadLeft(4, '0');
            File.Copy(assemblyLocation + pathToLog + fileName,
                assemblyLocation + pathToLog + archiveDirectory + "\\Log_" + numNextFileStr + "_" +
                DateTime.UtcNow.ToString("ddMMyyyyHHmmss") + ".log");
        }

        /****************PRIVATE LOGGER***************/
        private int parseLeveltoInt(string level)
        {
            switch (level)
            {
                case "TRACE": return 0;
                case "DEBUG": return 1;
                case "INFO": return 2;
                case "WARNING": return 3;
                case "ERROR": return 4;
                case "FATAL": return 5;
                default: return -1;
            }
        }
        private bool checklevel(string lvl)
        {
            if (parseLeveltoInt(curLevel) > parseLeveltoInt(lvl)) return false;
            else return true;
        }
        private static void saveToFile(string message, string lvl, string clsname, DateTime date)
        {
            bool rewrite = true;
            try
            {
                FileInfo file = new FileInfo(assemblyLocation + pathToLog + fileName);
                sizeFileLog = file.Length;
            }
            catch (Exception) { rewrite = true; }
            if (sizeFileLog >= SettingsManager.getCurrentSettings().Log.LogFileSize)
            {
                writer.Close();
                writer.Dispose();
                writer = null;
                if (archive == true)
                {
                    try { archiveControl(); }
                    catch (Exception) { }
                }
                rewrite = false;
            }
            string errorText = lvl + "\t" +
                               date.ToString("dd/MM/yyyy HH:mm:ss.fff") + "\t" +
                               clsname + "\t" +
                               message;
            if (writer == null) writer = new StreamWriter(assemblyLocation + pathToLog + fileName, rewrite);
            writer.WriteLine(errorText);
            writer.Close();
            writer = null;
        }
        private static void queueController()
        {
            try
            {
                while (!stopRunner)
                {
                    if (messageQueue.Count != 0)
                    {
                        LogInfo info = null;
                        if (messageQueue.TryDequeue(out info))
                        {
                            saveToFile(info.msg, info.lvl, info.classname, info.date);
                        }

                    }
                    else Thread.Sleep(10);
                }
            }
            catch (ThreadAbortException)
            {
                //Thread.ResetAbort();
            }
        }
        private void addQueue(string msg, string lvl)
        {
            messageQueue.Enqueue(new LogInfo(msg, lvl, this.className, DateTime.Now));
        }
        /*************************PUBLIC LOGGER****************************/
        public void trace(string msg)
        {
            string typelevel = "TRACE";
            if (checklevel(typelevel) == true) addQueue(msg, typelevel);
        }
        public void debug(string msg)
        {
            string typelevel = "DEBUG";
            if (checklevel(typelevel) == true) addQueue(msg, typelevel);
        }
        public void info(string msg)
        {
            string typelevel = "INFO";
            if (checklevel(typelevel) == true) addQueue(msg, typelevel);
        }
        public void warning(string msg)
        {
            string typelevel = "WARNING";
            if (checklevel(typelevel) == true) addQueue(msg, typelevel);
        }
        public void error(string msg)
        {
            string typelevel = "ERROR";
            if (checklevel(typelevel) == true) addQueue(msg, typelevel);
        }
        public void fatal(string msg)
        {
            string typelevel = "WARNING";
            if (checklevel(typelevel) == true) addQueue(msg, typelevel);
        }
    }
}
