﻿using BreakoutControl.Graphics;
using BreakoutControl.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BreakoutControl
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            SettingsManager.CreateSettings();
            ModelManager.CreateSettings();
            Model dm = ModelManager.getCurrentSettings();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MainForm mf = new MainForm(dm);
            new Driver().Start();
            Application.Run(mf);
        }
    }
}
