﻿using BreakoutControl.Settings;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BreakoutControl
{
    public static class Config
    {
        //COLOR LEDS
        static string unknown = SettingsManager.getCurrentSettings().Resources.LedColorStateMc.Unknown;
        static string stopped = SettingsManager.getCurrentSettings().Resources.LedColorStateMc.Stopped;
        static string played = SettingsManager.getCurrentSettings().Resources.LedColorStateMc.Played;
        static string offline = SettingsManager.getCurrentSettings().Resources.LedColorStateMc.Offline;
        static string activeTask = SettingsManager.getCurrentSettings().Resources.LedColorTask.Active;
        static string disactiveTask = SettingsManager.getCurrentSettings().Resources.LedColorTask.Disactive;

        //IMAGES
        public static Image playedLed = (Image)Properties.Resources.ResourceManager.GetObject(Convert.ToString(played));
        public static Image unknownLed = (Image)Properties.Resources.ResourceManager.GetObject(Convert.ToString(unknown));
        public static Image offlineLed = (Image)Properties.Resources.ResourceManager.GetObject(Convert.ToString(offline));
        public static Image stoppedLed = (Image)Properties.Resources.ResourceManager.GetObject(Convert.ToString(stopped));
        public static Image activeTaskLed = (Image)Properties.Resources.ResourceManager.GetObject(Convert.ToString(activeTask));
        public static Image disactiveTaskLed = (Image)Properties.Resources.ResourceManager.GetObject(Convert.ToString(disactiveTask));

        //STATE CONSOLE TEXT
        public static string unknownTxt = SettingsManager.getCurrentSettings().Resources.StateMC.Unknown;
        public static string offlineTxt = SettingsManager.getCurrentSettings().Resources.StateMC.Offline;
        public static string playedTxt = SettingsManager.getCurrentSettings().Resources.StateMC.Played;
        public static string stoppedTxt = SettingsManager.getCurrentSettings().Resources.StateMC.Stopped;

        //TASK STATE TXT
        public static string activeTxt = SettingsManager.getCurrentSettings().Resources.LabelState.Active;
        public static string disactiveTxt = SettingsManager.getCurrentSettings().Resources.LabelState.NotActive;

    }
}
