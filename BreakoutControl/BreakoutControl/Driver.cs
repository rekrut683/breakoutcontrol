﻿using BreakoutControl.Controllers;
using BreakoutControl.Graphics;
using BreakoutControl.Settings;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BreakoutControl
{
    public class Driver
    {
        private static InfoConsole console;
        private static Logger log;

        private static SerialPort serial;
        private static bool isLoop = true;
        private static bool statusConnection = false;
        private static bool isReceive = false;
        private static int readTimeout = SysSettingsController.readtimeout;

        public static Action<bool> callbackChangeBtn = null;
        public static Func<GameInstance, byte[], IEnumerable<byte[]>> SendAction = sendCommands;

        private Thread poller;
        private int loopTimer = SysSettingsController.polltimeout;
        private int timeoutCommand = SysSettingsController.timeoutcommand;     
        private string portname;
        private int baudrate;
        private bool demo = SettingsManager.getCurrentSettings().Demo;
        
        /********************PUBLIC PROPS**************************/
        public static bool IsLoop
        {
            get { return isLoop; }
            set { isLoop = value; }
        }

        /********************CONSTRUCTOR***************************/
        public Driver()
        {
            log = new Logger(this.GetType().ToString());
            log.info("start driver instance");
            this.portname = SysSettingsController.serialport;
            this.baudrate = SysSettingsController.baudrate;
            console = MainForm.IConsole;
        }

        /********************PUBLIC METHODS*************************/
        public static string[] GetPorts()
        {
            string[] ports = SerialPort.GetPortNames();
            return ports;
        }
        public static string[] GetBauds()
        {
            return new string[3] { "9600", "57600", "115200" };
        }
        public void Start()
        {
            string demoStr = (demo) ? "demo game" : "";
            console.add("start "+demoStr);
            if (demo)
            {
                SendAction = sendCommandsDemo;
            }
            else
            {
                serial = new SerialPort();
                serial.PortName = portname;
                serial.BaudRate = baudrate;
                int count = 0;
                while (true)
                {
                    Thread.Sleep(100);
                    try { serial.Open(); }
                    catch { }
                    count++;
                    if (serial.IsOpen || count == 5) break;
                }
                if (count == 5)
                {
                    if (!demo)
                    {
                        console.add("Error open serial port, please set the correct port number and restart app");
                        return;
                        //throw new SerialOpenEx("error open serial port");
                    }
                }
                SendAction = sendCommands;
                console.add("Serial port "+serial.PortName+" is open");
            }
            statusConnection = true;
            try
            {
                poller = new Thread(loop);
                poller.IsBackground = true;
                poller.Start();
            }
            catch (Exception)
            {
                throw new ThreadStartEx("cannot start main loop");
            }
        }
        public static void setCallback(Action<bool> action)
        {
            callbackChangeBtn = action;
        }

        /********************PRIVATE METHODS*************************/
        private bool TreatmentRec(GameInstance gi, byte[] rec)
        {
            ProtoController._REQUEST req = new ProtoController._REQUEST(rec);
            string type = req.GetTypeRec();
            string cmd = gi.CommandProcess.Command;
            //console.add("command byte: " + BitConverter.ToString(rec));
            GameInstance.COMMAND_PROCESS prc = gi.CommandProcess.Process;
            bool conformity = false;
            if (cmd == type &&
                prc == GameInstance.COMMAND_PROCESS.INIT) conformity = true;
            if (conformity)
            {
                if (type == ProtoController.cmds.Check.name)
                {
                    gi.State = req.getState();
                }
                else if (type == ProtoController.cmds.Start.name) gi.State = GameInstance.MC_STATE.PLAYED;
                else if (type == ProtoController.cmds.Stop.name ||
                         type == ProtoController.cmds.Restart.name)
                {
                    gi.State = GameInstance.MC_STATE.STOPPED;
                    //gi.Time.Default();
                    gi.ActionProcess.ClearAction();
                    //if(callbackChangeBtn!=null) callbackChangeBtn(false);
                }
                else if (type == ProtoController.cmds.Execute.name)
                {
                    console.add(gi.Name + ": your command is executed");
                    return true;
                }
                else if (type == ProtoController.cmds.News.name)
                {
                    GameInstance.MC_STATE st = req.getState();
                    gi.State = st;
                    Dictionary<string, int> time = req.getTime();
                    Dictionary<int, int[]> data = req.getData();
                    Dictionary<int, bool> presentData = new Dictionary<int, bool>();
                    int shift = 8;
                    for (int i = 0; i < data.Count; i++)
                    {
                        for (int j = 0; j < data[i].Length; j++)
                        {
                            int val = i * shift + j;
                            presentData.Add(val, Convert.ToBoolean(data[i][j]));
                        }
                    }
                    foreach (GameTasks gt in gi.TaskList)
                    {
                        int id = gt.IdTask;
                        bool bTask = presentData[id - 1];
                        if (bTask != gt.State) gt.State = bTask;
                    }
                    gi.Time.setTimes(time["minutes"], time["seconds"]);
                }
                return true;
            }
            return false;
        }
        private void CheckCmds()
        {
            bool isUse = false;
            //console.addToInfoList("loop ....");
            foreach (GameInstance gi in Instances.Games.Values)
            {
                //if (!gi.ControlCommands.IsEmpty)
                while(!gi.ControlCommands.IsEmpty)
                {
                    //console.add("yet another commands from gi: "+gi.Name);
                    isUse = true;
                    //console.addToInfoList("game id is: " + gi.IdGame);
                    ControlCommands cc = null;
                    while (!gi.ControlCommands.TryDequeue(out cc)) ;
                    //console.addToInfoList("cmd is: " + cc.Type + ", cmd: " + cc.Commands);
                    int adr = gi.McAdr;
                    byte cmd = cc.Commands;
                    gi.CommandProcess.InitCommand(cc.Type);
                    //console.add("type: " + cc.Type);
                    List<byte[]> rec = SendAction(gi, new byte[] { cmd }).ToList();
                    //console.add("length: " + rec.Count);
                    //List<byte[]> rec = sendCommands(gi, new byte[] { cmd }).ToList();
                    foreach (byte[] bb in rec)
                    {
                        if(TreatmentRec(gi, bb)) gi.ActionProcess.ClearAction();
                    }
                }
            }
            if (isUse && 
                PointerPoller.Length > 0) Thread.Sleep(timeoutCommand);
        }
        private void CheckPoll()
        {
            if (PointerPoller.Length > 0)
            {
                int id = PointerPoller.GetNext();
                GameInstance gi = Instances.Games[id];
                if(/*gi.State == GameInstance.MC_STATE.OFFLINE ||
                   gi.State == GameInstance.MC_STATE.STOPPED ||
                   (gi.State == GameInstance.MC_STATE.PLAYED && 
                    gi.ActionProcess.Action == ProtoController.actions.Disconnect)*/
                    gi.ActionProcess.Action == ProtoController.actions.Disconnect)
                {
                    //if (callbackChangeBtn != null) callbackChangeBtn(false);
                    foreach (ControlsBox.ButtonControls bc in gi.CtrlBtns) bc.onChangeFn(false);
                    PointerPoller.Remove(id);
                    gi.ActionProcess.ClearAction();
                    return;
                }
                string commandname = "";
                if(gi.State == GameInstance.MC_STATE.UNKNOWN &&
                   gi.CommandProcess.Command != ProtoController.cmds.Check.name)
                {
                    commandname = ProtoController.cmds.Check.name;
                    byte cmd = (byte)ProtoController.cmds.Check.TxCmd;
                    ControlCommands cCommand = new ControlCommands(commandname, cmd);
                    gi.ControlCommands.Enqueue(cCommand);
                }
                if (gi.State == GameInstance.MC_STATE.PLAYED ||
                    gi.State == GameInstance.MC_STATE.STOPPED ||
                    gi.State == GameInstance.MC_STATE.OFFLINE)
                {
                    int adr = gi.McAdr;
                    commandname = ProtoController.cmds.News.name;
                    byte commnd = (byte)ProtoController.cmds.News.TxCmd;
                    byte[] snd = new byte[] { commnd };
                    List<byte[]> rec = SendAction(gi, snd).ToList();
                    if (rec.Count > 0) gi.Progress.MoveBar();
                    foreach (byte[] b in rec) TreatmentRec(gi, b);
                }
                gi.CommandProcess.InitCommand(commandname);
                if (gi.ActionProcess.Action == ProtoController.actions.Auto)
                {
                    foreach (ControlsBox.ButtonControls cb in gi.CtrlBtns) cb.onChangeFn(true);
                    //callbackChangeBtn(true);
                    gi.ActionProcess.ClearAction();
                }
            }
        }
        private void loop()
        {
            while (isLoop)
            {
                CheckCmds();
                CheckPoll();
                Thread.Sleep(loopTimer);
            }
            if (serial != null && serial.IsOpen) serial.Close();
        }
        private static IEnumerable<byte[]> sendCommandsDemo(GameInstance gi, byte[] comm)
        {
            foreach (byte b in comm) yield return TestClass.GetRequest(b);
        }
        private static IEnumerable<byte[]> sendCommands(GameInstance gi, byte[] comm)
        {
            foreach (byte b in comm)
            {
                byte[] sendout = new ProtoController._SEND(gi.McAdr, b).getBytes();
                
                //console.add("Tx: " + BitConverter.ToString(sendout));
                try
                {
                    serial.DiscardInBuffer();
                    serial.Write(sendout, 0, sendout.Length);
                }
                catch (Exception e)
                {
                    //console.add("ex: " + e);
                    statusConnection = false;
                }
                isReceive = true;
                byte[] receiveBytes = onReceiveSerial(gi);
                //if (receiveBytes != null) 
                //    console.add("Rx: " + BitConverter.ToString(receiveBytes));
                if (receiveBytes == null ||
                    receiveBytes.Length <= 0 ||
                    !statusConnection)
                {
                    //console.add("receive is null : " + gi.IdGame);
                    if (gi.Attempt == SysSettingsController.attemptreceipt)
                    {
                        gi.State = GameInstance.MC_STATE.OFFLINE;
                        //gi.ClearCommands();
                        gi.Attempt = 0;
                    }
                    else gi.Attempt++;
                    yield break;
                }
                if (gi.Attempt != 0) gi.Attempt = 0;
                //console.add("Rx: " + BitConverter.ToString(receiveBytes));
                yield return receiveBytes;
            }
        }
        private static byte[] onReceiveSerial(GameInstance gi)
        {
            List<byte> incomeBytes = new List<byte>();
            int count = 0;
            byte[] buff = new byte[24];
            int len = 0;
            int full_len = 0;
            bool isLen = false;
            int counter_read = 0;
            serial.ReadTimeout = readTimeout;
            try
            {
                while (isReceive)
                {
                    count = 0;
                    counter_read++;
                    if (counter_read >= readTimeout / 10) return null;
                    Thread.Sleep(10);
                    byte b = 0;
                    try
                    {
                        b = (byte)serial.ReadByte();
                        //console.add("byte: " + b);
                    }
                    catch(Exception ex)
                    { 
                        return null;
                    }
                    incomeBytes.Add(b);
                    if (incomeBytes.Count > 4 && !isLen)
                    {
                        len = incomeBytes[3];
                        full_len = len;
                        isLen = true;
                        if (count == full_len) isReceive = false;
                    }
                    else if (isLen)
                    {
                        if (incomeBytes.Count == full_len) { isReceive = false; }
                    }
                }
                return incomeBytes.ToArray();
            }
            catch (Exception e)
            {
                return null;
            }
        }

    }
}
