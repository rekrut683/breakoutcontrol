﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BreakoutControl
{
    public class SerialOpenEx:Exception
    {
        public SerialOpenEx(string msg) : base(msg) { }
    }
    public class ThreadStartEx : Exception
    {
        public ThreadStartEx(string msg) : base(msg) { }
    }
}
