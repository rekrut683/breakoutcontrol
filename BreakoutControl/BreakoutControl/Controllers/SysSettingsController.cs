﻿using BreakoutControl.Graphics;
using BreakoutControl.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BreakoutControl.Controllers
{
    public static class SysSettingsController
    {
        public static DefaultSettings.Settings.SYSTEMSETTINGS syssettings = SettingsManager.getCurrentSettings().SystemSettings;

        public static string serialport = syssettings.SerialPort.defaultvalue;
        public static int baudrate = Convert.ToInt32(syssettings.BaudRate.defaultvalue);
        public static int polltimeout = Convert.ToInt32(syssettings.TimeoutPoller.defaultvalue);
        public static int attemptreceipt = Convert.ToInt32(syssettings.AttemptPoller.defaultvalue);
        public static int sizeinfoconsole = Convert.ToInt32(syssettings.SizeConsole.defaultvalue);
        public static int readtimeout = Convert.ToInt32(syssettings.ReadTimeout.defaultvalue);
        public static int timeoutcommand = Convert.ToInt32(syssettings.TimeoutCommand.defaultvalue);

    }
}
