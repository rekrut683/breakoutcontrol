﻿using BreakoutControl.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BreakoutControl.Controllers
{
    public class GameTasks
    {
        private static InfoConsole console;
        /*******************PRIVATE FIELDS************************/
        private ContainerTask containerTask;
        private string name;
        private int idtask;
        private int gameid;
        /*******************PUBLIC PROPS**************************/
        public bool State
        {
            get { return this.containerTask.getState(); }
            set
            {
                if (this.containerTask.getState() != value)
                {
                    var gi = Instances.Games[gameid];
                    string gamename = gi.Name;
                    string taskname = this.Name;
                    string val = "";
                    if (value) val = GraphicController.iConsole.PositivePhrase;
                    else val = GraphicController.iConsole.NegativePhrase;
                    console.add(gamename + ": " + taskname + " " + val);
                }
                this.containerTask.ChangeLeds(value);
            }
        }
        public string Name
        {
            get { return name; }
        }
        public int IdTask
        {
            get { return idtask; }
        }
        /********************CONSTRUCTOR****************************/
        public GameTasks(ContainerTask taskForm, int gid)
        {
            this.containerTask = taskForm;
            this.name = taskForm.NameTask;
            this.idtask = taskForm.Id;
            this.gameid = gid;
            MainForm.onConsoleReady += new OnConsoleReady(() => { console = MainForm.IConsole; });
        }
    }
}
