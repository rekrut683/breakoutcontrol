﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BreakoutControl.Controllers
{
    public class ControlCommands
    {
        private string type;
        private byte commands;

        public string Type
        {
            get { return this.type; }
        }
        public byte Commands
        {
            get { return this.commands; }
        }

        public ControlCommands(string type, byte commands)
        {           
            if (type == null)
                this.type = ProtoController.cmds.Execute.name;
            else this.type = type;
            this.commands = commands;
        }
    }
}
