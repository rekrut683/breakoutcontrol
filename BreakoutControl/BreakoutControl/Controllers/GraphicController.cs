﻿using BreakoutControl.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BreakoutControl.Controllers
{
    public static class GraphicController
    {
        public static DefaultSettings.Settings.MAINFORM mForm = SettingsManager.getCurrentSettings().MainForm;
        public static DefaultSettings.Settings.MAINFORM.INFOCONSOLE iConsole = mForm.InfoConsole;
        public static DefaultSettings.Settings.MAINFORM.SETTINGSTAB settTab = mForm.SettingsDiag;
        public static DefaultSettings.Settings.MAINFORM.STATECONSOLE stateConsole = mForm.StateConsole;
        public static DefaultSettings.Settings.MAINFORM.TABPANEL tPanel = mForm.TabPanel;
        public static DefaultSettings.Settings.MAINFORM.TABS tabs = mForm.Tabs;

        public static DefaultSettings.Settings.MAINFORM.TABS.CONTROLPANEL cPanel = tabs.ControlPanel;
        public static DefaultSettings.Settings.MAINFORM.TABS.GAMELAYOUT gLay = tabs.GameLayout;
        public static DefaultSettings.Settings.MAINFORM.TABS.SECTION sections = tabs.Section;

        public static DefaultSettings.Settings.MAINFORM.TABS.SECTION.ACTIONCONTAINER actions = sections.ActionContainer;
        public static DefaultSettings.Settings.MAINFORM.TABS.SECTION.TASKCONTAINER tasks = sections.TaskControl;

        public static DefaultSettings.Settings.MAINFORM.TABS.SECTION.ACTIONCONTAINER.BUTTON btns = actions.Button;

        public static DefaultSettings.Settings.MAINFORM.TABS.SECTION.TASKCONTAINER.EVENT events = tasks.EventContainer;

        public static DefaultSettings.Settings.MAINFORM.TABS.CONTROLPANEL.CHECK checkConsole = cPanel.CheckConsole;
        public static DefaultSettings.Settings.MAINFORM.TABS.CONTROLPANEL.TIME timeConsole = cPanel.TimeConsole;
        public static DefaultSettings.Settings.MAINFORM.TABS.CONTROLPANEL.PROGRESS progress = cPanel.ProgressConsole;

    }
}
