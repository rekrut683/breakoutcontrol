﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BreakoutControl.Controllers
{
    public static class PointerPoller
    {
        /**********************PRIVATE FIELDS***********************/
        private static List<int> ids = new List<int>();
        private static int index = -1;

        /*****************PUBLIC PROPS******************************/
        public static int Length
        {
            get { return ids.Count; }
        }
        public static void Add(int i)
        {
            ids.Add(i);
        }
        public static bool Contains(int i)
        {
            return ids.Contains(i);
        }
        public static int GetNext()
        {
            if(ids.Count > 0)
            {
                int nextIndex = index + 1;
                if (nextIndex >= ids.Count) index = 0;
                else index = nextIndex;
                return ids[index];
            }
            return -1;
        }
        public static void Remove(int i)
        {
            ids.Remove(i);
        }

    }
}
