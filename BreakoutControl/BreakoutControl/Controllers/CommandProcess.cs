﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreakoutControl.Controllers
{
    public class CommandProcess
    {
        private string command = null;
        private GameInstance.COMMAND_PROCESS process;

        public string Command
        {
            get { return this.command; }
        }
        public GameInstance.COMMAND_PROCESS Process
        {
            get { return this.process; }
        }

        public void InitCommand(string cmd)
        {
            this.command = cmd;
            this.process = GameInstance.COMMAND_PROCESS.INIT;
        }
        public void ProcessingCommand()
        {
            this.process = GameInstance.COMMAND_PROCESS.PROCESS;
        }
        public void FinalCommand()
        {
            this.process = GameInstance.COMMAND_PROCESS.FINAL;
        }
        public void ClearCommand()
        {
            this.process = GameInstance.COMMAND_PROCESS.NONE;
            this.command = null;
        }
    }
}
