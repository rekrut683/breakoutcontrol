﻿using BreakoutControl.Settings;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace BreakoutControl.Controllers
{
    public static class ProtoController
    {
        private static DefaultSettings.Settings.PROTOCONSTANTS proto = SettingsManager.getCurrentSettings().ProtoConstants;

        public static DefaultSettings.Settings.PROTOCONSTANTS.COMMANDS cmds = proto.Commands;
        public static DefaultSettings.Settings.PROTOCONSTANTS.ACTIONBOOK actions = proto.ActionBook;

        /*******************PUBLIC METHODS********************/
        public static byte GetSendCmd(string action)
        {
            return GetCmd(action, "TxCmd");
        }
        public static byte GetReceiveCmd(string action)
        {
            return GetCmd(action, "RxCmd");
        }
        public static Dictionary<string, byte> FillCommands()
        {
            Dictionary<string, byte> resDct = new Dictionary<string, byte>();
            object res = ControllerUtils.GetInnerFields(cmds, "TxCmd");
            Dictionary<string, object> dct = res as Dictionary<string, object>;
            foreach (KeyValuePair<string, object> kp in dct) resDct.Add(kp.Key, Convert.ToByte(kp.Value));
            return resDct;
        }

        /*********************PRIVATE METHODS***************************/
        private static byte GetCmd(string action, string field)
        {
            object res = ControllerUtils.GetInnerFields(cmds, field);
            Dictionary<string, object> resDct = res as Dictionary<string, object>;
            return Convert.ToByte(resDct[action]);
        }

        /*****************CLASS REQUEST**************************/
        public class _REQUEST
        {
            /********************PRIVATE FIELDS******************/
            private byte[] req;

            /*******************CONSTRUCTOR**********************/
            public _REQUEST(byte[] b) { this.req = b; }

            /******************PUBLIC METHODS********************/
            public string GetTypeRec()
            {
                if (this.req.Length > 5)
                {
                    byte command = req[4];
                    object res = ControllerUtils.GetInnerFields(cmds, "RxCmd");
                    Dictionary<string, object> resDct = res as Dictionary<string, object>;
                    foreach (KeyValuePair<string, object> kp in resDct)
                    {
                        if (command == Convert.ToByte(kp.Value)) return kp.Key;
                    }
                }
                return null;
            }
            
            public GameInstance.MC_STATE getState()
            {
                int len = req[3];
                if (len < 7) return GameInstance.MC_STATE.UNKNOWN;
                int st = req[5];
                if (st == 0) return GameInstance.MC_STATE.STOPPED;
                else if (st == 1) return GameInstance.MC_STATE.PLAYED;
                else return GameInstance.MC_STATE.UNKNOWN;
            }
            public Dictionary<int, int[]> getData()
            {
                int len = req[3];
                if (len < 8) return null;
                else
                {
                    byte[] data = new byte[3] { req[6], req[7], req[8] };
                    Dictionary<int, int[]> result = new Dictionary<int, int[]>();
                    int counter = 0;
                    foreach (byte b in data)
                    {
                        int[] bites = new int[8];
                        for (int i = 0; i < 8; i++)
                        {
                            int bit = 0;
                            bit = (b >> i) & 0x01;
                            bites[i] = bit;
                        }
                        result.Add(counter, bites);
                        counter++;
                    }
                    return result;
                }
            }
            public Dictionary<string, int> getTime()
            {
                int len = req[3];
                if (len < 8) return null;
                else
                {
                    Dictionary<string, int> result = new Dictionary<string, int>();
                    byte[] time = new byte[4] { req[9], req[10], req[11], 0 };
                    int time_long = BitConverter.ToInt32(time, 0);
                    result.Add("minutes", (time_long / 1000) / 60);
                    result.Add("seconds", (time_long / 1000) % 60);
                    return result;
                }
            }

        }

        /*****************CLASS SEND******************************/
        public class _SEND
        {
            /**************PRIVATE FIELDS*********************/
            private List<byte> send_out = new List<byte>();

            /**************CONSTRUCTOR************************/
            public _SEND(int adr, byte comm)
            {
                this.send_out.Add((byte)proto.FrameController);
                this.send_out.Add((byte)proto.SelfId);
                this.send_out.Add((byte)adr);
                this.send_out.Add(comm);
                this.send_out.Add((byte)proto.FrameController);
            }

            /**************PUBLIC METHODS*********************/
            public byte[] getBytes()
            {
                return this.send_out.ToArray();
            }
        }
    }
}