﻿using BreakoutControl.Graphics;
using BreakoutControl.Settings;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BreakoutControl.Graphics.ControlsBox;

namespace BreakoutControl.Controllers
{
    public class GameInstance
    {
        public enum MC_STATE
        {
            UNKNOWN = -1,
            OFFLINE,
            STOPPED,
            PLAYED
        }
        public enum COMMAND_PROCESS
        {
            NONE,
            INIT,
            PROCESS,
            FINAL
        }
        private static InfoConsole console;
        /******************PRIVATE FIELDS********************/
        private MC_STATE newState = MC_STATE.UNKNOWN;
        private MC_STATE state = MC_STATE.UNKNOWN;
        private int idgame;
        private int mcadr;
        private int attempt;
        private string name;

        /****************CMD & ACTION PROCESS GAME****************/
        public CommandProcess CommandProcess = new CommandProcess();
        public ActionProcess ActionProcess = new ActionProcess();

        /****************QUEUE CMDS******************************/
        public ConcurrentQueue<ControlCommands> ControlCommands = new ConcurrentQueue<ControlCommands>();

        /*****************TASK LIST*****************************/
        public List<GameTasks> TaskList = new List<GameTasks>();

        /*****************CONTROL_BUTTONS_LIST******************/
        public List<ButtonControls> CtrlBtns = new List<ButtonControls>();

        /*****************CHECK & TIME FORM CONTROLS****************/
        public TimeConsole Time;
        public CheckConsole CheckConsole;
        public ProgressConsole Progress;

        /*****************PUBLIC PROPERTIES*************************/
        public MC_STATE State
        {
            get { return this.state; }
            set
            {
                if (this.state != value) console.add(this.name + " has state changed to: " + value);
                this.state = value;
                Check(value);
            }
        }
        public MC_STATE NewState
        {
            get { return newState; }
            set { newState = value; }
        }
        public int Attempt
        {
            get { return attempt; }
            set { attempt = value; }
        }
        public int IdGame
        {
            get { return this.idgame; }
        }
        public int McAdr
        {
            get { return this.mcadr; }
        }
        public string Name
        {
            get { return name; }
        }

        /*********************CONSTRUCTOR****************************/
        public GameInstance(int id, int adr, string name)
        {
            this.idgame = id;
            this.mcadr = adr;
            this.name = name;
            MainForm.onConsoleReady += new OnConsoleReady(()=> { console = MainForm.IConsole; });
        }
        

        /*********************PUBLIC METHODS*************************/
        public void ActionFForm(string act, byte? cmd = null)
        {
            string commnd = null;
            //ActionProcess.InitAction(act);
            if (act == ProtoController.actions.Auto) PointerPoller.Add(this.idgame);
            else if (act == ProtoController.actions.Disconnect) { }
            else if (act == ProtoController.actions.Manual) commnd = ProtoController.cmds.News.name;
            else if (act == ProtoController.actions.Ping) commnd = ProtoController.cmds.Check.name;
            else if (act == ProtoController.actions.Restart ||
                     act == ProtoController.actions.Start ||
                     act == ProtoController.actions.Stop ||
                     act == ProtoController.actions.Execute) commnd = act;
            else return;
            if (commnd == null) return;
            if (this.State == MC_STATE.UNKNOWN ||
                this.State == MC_STATE.OFFLINE)
                this.ControlCommands.Enqueue(new ControlCommands(ProtoController.cmds.Check.name, (byte)ProtoController.cmds.Check.TxCmd));
            if (act == ProtoController.actions.Execute)
                this.ControlCommands.Enqueue(new ControlCommands(commnd, cmd.GetValueOrDefault()));
            else if (commnd != ProtoController.cmds.Check.name)
                this.ControlCommands.Enqueue(new ControlCommands(commnd, ProtoController.GetSendCmd(commnd)));
        }
        public void ClearCommands()
        {
            foreach (ControlCommands cc in ControlCommands)
            {
                ControlCommands cCommand = null;
                while (!ControlCommands.TryDequeue(out cCommand)) ;
            }
        }

        /*********************PRIVATE METHODS**************************/
        private void Check(MC_STATE state)
        {
            this.CheckConsole.changeStateCheck((int)state);
            MainForm.StateConsole.changeState((int)state, this.idgame);
            if (state == MC_STATE.OFFLINE) ClearCommands();
        }

    }
}
