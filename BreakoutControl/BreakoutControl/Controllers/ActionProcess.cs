﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BreakoutControl.Controllers
{
    public class ActionProcess
    {
        private string action = null;
        private GameInstance.COMMAND_PROCESS process;

        public string Action
        {
            get { return this.action; }
        }
        public GameInstance.COMMAND_PROCESS Process
        {
            get { return this.process; }
        }

        public void InitAction(string act)
        {
            this.action = act;
            this.process = GameInstance.COMMAND_PROCESS.INIT;
        }
        public void ProcessingAction()
        {
            this.process = GameInstance.COMMAND_PROCESS.PROCESS;
        }
        public void FinalAction()
        {
            this.process = GameInstance.COMMAND_PROCESS.FINAL;
        }
        public void ClearAction()
        {
            this.process = GameInstance.COMMAND_PROCESS.NONE;
            this.action = null;
        }
    }
}
