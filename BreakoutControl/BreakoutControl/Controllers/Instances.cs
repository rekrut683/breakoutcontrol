﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BreakoutControl.Controllers
{
    public static class Instances
    {
        public static Dictionary<int, GameInstance> Games = new Dictionary<int, GameInstance>();
        public static Dictionary<string, byte> ControlsCommands = new Dictionary<string, byte>();
    }
}
