﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BreakoutControl.Controllers
{
    public static class ControllerUtils
    {
        public static FieldInfo[] GetFields(object obj)
        {
            return obj.GetType().GetFields();
        }
        public static object GetInnerFields(object outObj, params string[] str)
        {
            Dictionary<string, object> res = new Dictionary<string, object>();
            Dictionary<string, object> localRes = null;
            int depth;
            if (str.Length == 0)
            {
                depth = 0;
                FieldInfo[] fi = outObj.GetType().GetFields();
                List<string> listStr = new List<string>();
                foreach (FieldInfo f in fi)
                {
                    object o = f.GetValue(outObj);
                    foreach(FieldInfo ff in GetFields(o))
                    {
                        listStr.Add(ff.Name);
                    }
                    break;
                }
                str = listStr.ToArray();
            }
            else if (str.Length == 1) depth = 1;
            else depth = -1;
            foreach (FieldInfo f in GetFields(outObj))
            {
                object obj = f.GetValue(outObj);
                Type type = obj.GetType();
                if (depth != 1) localRes = new Dictionary<string, object>();
                foreach(string s in str)
                {
                    FieldInfo fInfo = type.GetField(s);
                    if (fInfo != null)
                    {
                        if (depth != 1) localRes.Add(s, fInfo.GetValue(obj));
                        else res.Add(type.GetField("name").GetValue(obj).ToString(), fInfo.GetValue(obj));
                    }
                }
                if (depth != 1) res.Add(f.Name.ToUpper(), localRes);
            }
            return res;
        }
    }
}
