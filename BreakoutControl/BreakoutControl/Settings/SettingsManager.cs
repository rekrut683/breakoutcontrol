﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BreakoutControl.Settings
{
    public static class SettingsManager
    {
        private static string path;
        private static string filename = "settings.json";
        static DefaultSettings.Settings Sett;

        public static void CreateSettings()
        {
            string assemblyName = Assembly.GetExecutingAssembly().GetName().Name;
            path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + assemblyName + "\\";
            DefaultSettings currentSettings = null;
            if (File.Exists(path + filename) == false)
            {
                currentSettings = new DefaultSettings();
                setSettings(currentSettings);
            }
            else
            {
                currentSettings = readSettings();
                if (currentSettings == null)
                {
                    currentSettings = new DefaultSettings();
                    setSettings(currentSettings);
                }
                else setSettings(currentSettings);
            }
            Sett = currentSettings.settings;
        }

        public static DefaultSettings.Settings getCurrentSettings()
        {
            return Sett;
        }

        private static DefaultSettings readSettings()
        {
            try
            {
                string settings = "";
                using (StreamReader readtext = new StreamReader(path + filename))
                {
                    string line;
                    while ((line = readtext.ReadLine()) != null) settings += line;
                }
                return JsonConvert.DeserializeObject<DefaultSettings>(settings);
            }
            catch (Exception) { return null; }
        }

        private static void setSettings(DefaultSettings ds)
        {
            string settings = JsonConvert.SerializeObject(ds, Formatting.Indented);
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
            using (StreamWriter sw = new StreamWriter(path + filename, false))
            {
                sw.WriteLine(settings);
                sw.Close();
            }

        }
    }
}
