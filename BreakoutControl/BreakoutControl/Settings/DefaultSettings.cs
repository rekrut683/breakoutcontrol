﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BreakoutControl.Settings
{
    public class DefaultSettings
    {
        public Settings settings = new Settings();
        public class Settings
        {
            public bool Demo = false;
            public MAINFORM MainForm = new MAINFORM();
            public RESOURCES Resources = new RESOURCES();
            public SYSTEMSETTINGS SystemSettings = new SYSTEMSETTINGS();
            public PROTOCONSTANTS ProtoConstants = new PROTOCONSTANTS();
            public LOG Log = new LOG();
            public class LOG
            {
                public string Level = "INFO";
                public string PathToLog = ".\\";
                public string Filename = "log.txt";
                public bool Archive = true;
                public bool Zip = true;
                public int MaxArchiveFiles = 20;
                public int CleanupArchiveLimit = 10;
                public int LogFileSize = 5000000;
            }
            public class RESOURCES
            {
                public LEDCOLORMC LedColorStateMc = new LEDCOLORMC();
                public LABELSTATE LabelState = new LABELSTATE();
                public STATEMC StateMC = new STATEMC();
                public LEDCOLORTASK LedColorTask = new LEDCOLORTASK();
                public class LEDCOLORMC
                {
                    public string Offline = "test_red";
                    public string Played = "test_green";
                    public string Unknown = "test_orange";
                    public string Stopped = "test_blue";
                }
                public class LEDCOLORTASK
                {
                    public string Active = "test_green";
                    public string Disactive = "test_red";
                }
                public class LABELSTATE
                {
                    public string Active = "Activated";
                    public string NotActive = "Not Active";
                }
                public class STATEMC
                {
                    public string Unknown = "Undefined";
                    public string Stopped = "Stopped";
                    public string Played = "Played";
                    public string Offline = "Offline";
                }
            }
            public class MAINFORM
            {
                public int Width = 1200;
                public int Height = 800;
                public int StartPointX = 10;
                public int StartPointY = 10;
                public string Name = "Breakout Control";
                public TABPANEL TabPanel = new TABPANEL();
                public TABS Tabs = new TABS();
                public SETTINGSTAB SettingsDiag = new SETTINGSTAB();
                public INFOCONSOLE InfoConsole = new INFOCONSOLE();
                public STATECONSOLE StateConsole = new STATECONSOLE();
                public class TABPANEL
                {
                    public int PaddingX = 0;
                    public int PaddingY = 0;
                    public int Width = 1070;
                    public int LeftPosition = 10;
                }
                public class SETTINGSTAB
                {
                    public string Name = "Settings & Diagnostic";
                }
                public class TABS
                {
                    public int HeightTaskLayout = 520;
                    public GAMELAYOUT GameLayout = new GAMELAYOUT();
                    public SECTION Section = new SECTION();
                    public CONTROLPANEL ControlPanel = new CONTROLPANEL();
                    public class GAMELAYOUT
                    {
                        public int Height = 1070;
                    }
                    public class CONTROLPANEL
                    {
                        public TIME TimeConsole = new TIME();
                        public CHECK CheckConsole = new CHECK();
                        public PROGRESS ProgressConsole = new PROGRESS();
                        public int Height = 80;
                        public int Indent = 20;
                        public int Width = 1060;
                        public int InnerIndent = 10;
                        public int HeightControlItem = 50;
                        public int TopControlItem = 20;
                        public int IndentLeftControlItem = 10;
                        public class TIME
                        {
                            public string TextLabel = "Time Left";
                            public int TopTimeConsole = 20;
                            public int WidthTimeConsole = 200;
                            public int HeightTimeConsole = 50;
                            public string MinuteInitLabel = "60";
                            public string SecondsInitLabel = "00";
                            public int WidthPartTime = 35;
                            public int WidthSeparator = 15;
                            public int TopPartTime = 14;
                            public int HeightPartTime = 33;
                            public int PaddingTime = 10;
                            public int MarginTime = 0;
                            public float FontSize = 15F;
                            public string Color = "Red";
                            public int LeftPos = 50;
                        }
                        public class CHECK
                        {
                            public string TextLabel = "Check Console";
                            public int Top = 20;
                            public int Width = 100;
                            public int Height = 50;
                            public int LedRadius = 29;
                            public int TopLed = 16;
                            public int LeftLed = 33;
                        }
                        public class PROGRESS
                        {
                            public string TextLabel = "Progress";
                            public int Top = 20;
                            public int Width = 200;
                            public int Height = 50;
                            public int ProgressWidth = 192;
                            public int TopProgress = 16;
                            public int LeftProgress = 4;
                            public int ProgressHeight = 25;
                        }
                    }
                    public class SECTION
                    {
                        public int IndentTop = 10;
                        public int IndentLeft = 20;
                        public int Width = 190;
                        public TASKCONTAINER TaskControl = new TASKCONTAINER();
                        public ACTIONCONTAINER ActionContainer = new ACTIONCONTAINER();
                        public class TASKCONTAINER
                        {
                            public EVENT EventContainer = new EVENT();
                            public int StartHeight = 30;
                            public int Width = 190;
                            public int Height = 70;
                            public int IndentTop = 25;
                            public int IndentLeft = 10;
                            public class EVENT
                            {
                                public int Width = 160;
                                public int LedRadius = 34;
                                public int IndentTopEvent = 25;
                                public int IndentLedLocationX = 40;
                                public int IndentLedLocationY = 10;
                            }
                        }
                        public class ACTIONCONTAINER
                        {
                            public BUTTON Button = new BUTTON();
                            public int Height = 30;
                            public int Width = 160;
                            public int IndentLeft = 10;
                            public int IndentTop = 25;
                            public class BUTTON
                            {
                                public int Width = 150;
                                public int Indent = 0;
                            }
                        }
                    }
                }
                public class INFOCONSOLE
                {
                    public string Text = "Info Console";
                    public int Height = 150;
                    public int HeightIndent = 18;
                    public int WidthIndent = 10;
                    public int LeftPosition = 5;
                    public string PositivePhrase = "Activated";
                    public string NegativePhrase = "Not active";
                }
                public class STATECONSOLE
                {
                    public int Width = 100;
                    public int HeightIndent = 13;
                    public string Text = "State";
                    public int Top = 13;
                    public int BeginTop = 20;
                    public int NextIndent = 20;
                    public int LedRadius = 34;
                    public int IndentLedLeft = 30;
                    public int WidthLabel = 90;
                    public int HeightLabel = 20;
                    public int WidthStateLabel = 70;
                    public int LeftIndentLabel = 5;
                    public int IndentStateLabel = 20;
                    public int IndentStateConsole = 20;
                }
            }
            public class PROTOCONSTANTS
            {
                public int FrameController = 0x7E;
                public int SelfId = 0x01;
                public COMMANDS Commands = new COMMANDS();
                public ACTIONBOOK ActionBook = new ACTIONBOOK();
                public class COMMANDS
                {
                    public STARTCMD Start = new STARTCMD();
                    public STOPCMD Stop = new STOPCMD();
                    public RESTARTCMD Restart = new RESTARTCMD();
                    public CHECKCMD Check = new CHECKCMD();
                    public NEWSCMD News = new NEWSCMD();
                    public EXECUTECMD Execute = new EXECUTECMD();
                    public class STARTCMD
                    {
                        public string name = "START";
                        public int TxCmd = 0x11;
                        public int RxCmd = 0x21;
                    }
                    public class STOPCMD
                    {
                        public string name = "STOP";
                        public int TxCmd = 0x12;
                        public int RxCmd = 0x22;
                    }
                    public class RESTARTCMD
                    {
                        public string name = "RESTART";
                        public int TxCmd = 0x13;
                        public int RxCmd = 0x23;
                    }
                    public class CHECKCMD
                    {
                        public string name = "CHECK";
                        public int TxCmd = 0x51;
                        public int RxCmd = 0x72;
                    }
                    public class NEWSCMD
                    {
                        public string name = "NEWS";
                        public int TxCmd = 0x14;
                        public int RxCmd = 0x24;
                    }
                    public class EXECUTECMD
                    {
                        public string name = "EXECUTE";
                        public int TxCmd = 0xFF;
                        public int RxCmd = 0xCC;
                    }
                }
                public class ACTIONBOOK
                {
                    public string Start = "START";
                    public string Stop = "STOP";
                    public string Restart = "RESTART";
                    public string Ping = "PING";
                    public string Auto = "AUTO";
                    public string Manual = "MANUAL";
                    public string Execute = "EXECUTE";
                    public string Disconnect = "DISCONNECT";
                }
            }
            public class SYSTEMSETTINGS
            {
                public SERIALPORT SerialPort = new SERIALPORT();
                public BAUDRATE BaudRate = new BAUDRATE();
                public TIMEOUTPOLLER TimeoutPoller = new TIMEOUTPOLLER();
                public TIMEOUTCOMMAND TimeoutCommand = new TIMEOUTCOMMAND();
                public READTIMEOUT ReadTimeout = new READTIMEOUT();
                public ATTEMPTPOLLER AttemptPoller = new ATTEMPTPOLLER();
                public SIZEINFOCONSOLE SizeConsole = new SIZEINFOCONSOLE();
                public class SERIALPORT
                {
                    public string name = "SerialPort:";
                    public string type = "CMB";
                    public string defaultvalue = "COM10";
                }
                public class BAUDRATE
                {
                    public string name = "BaudRate Serial port:";
                    public string type = "CMB";
                    public string defaultvalue = "57600";
                }
                public class TIMEOUTPOLLER
                {
                    public string name = "Poller Timeout:";
                    public string type = "TXT";
                    public string defaultvalue = "200";
                }
                public class TIMEOUTCOMMAND
                {
                    public string name = "Timeout Commands";
                    public string type = "TXT";
                    public string defaultvalue = "500";
                }
                public class READTIMEOUT
                {
                    public string name = "SerialPort Read Timeout:";
                    public string type = "TXT";
                    public string defaultvalue = "500";
                }
                public class ATTEMPTPOLLER
                {
                    public string name = "Attmpt receipt poller:";
                    public string type = "TXT";
                    public string defaultvalue = "3";
                }
                public class SIZEINFOCONSOLE
                {
                    public string name = "Size of infoconsole:";
                    public string type = "TXT";
                    public string defaultvalue = "10";
                }
            }
            
        }
    }
}
